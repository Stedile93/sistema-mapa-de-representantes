<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CidadesRepresentante extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cidades_representante', function($table){

      $table->increments('id');

      $table->integer('id_usuario')->unsigned();
      $table->foreign('id_usuario')->references('id')->on('usuarios');
      $table->integer('id_cidade')->unsigned();
      $table->foreign('id_cidade')->references('id')->on('cidades');

      $table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cidades_representante');
	}

}
