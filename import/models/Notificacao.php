<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Notificacao extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'notificacoes';

}
