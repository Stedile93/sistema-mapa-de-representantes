@extends('layouts.master')


@section('javascripts')
   <script type="text/javascript">
   $(document).ready(function(){
      $('select#filtro-mapa').on('change', function(e){
         var filtroSelecionado = $(this).val();

         location.href="/filtra-mapa/"+filtroSelecionado+"";
      });

      $('select#filtro-representante').on('change', function(e){
         var filtroSelecionado = $(this).val();

         location.href="/filtra-mapa/"+filtroSelecionado+"";
      });
   });
   </script>

   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfGSKVmJhINg5yKyOLd3rQVlOg4V-NUGg"></script>
   <script type="text/javascript">
      $(window).resize(function () {
         var h = $(window).height(),
             offsetTop = 120;

         $('#mapa-google').css('height', (h - offsetTop));
      }).resize();

      var infowindow = null;
      var gmarkers   = [];

      var pinMap   = 'M0-165c-27.618 0-50 21.966-50 49.054C-50-88.849 0 0 0 0s50-88.849 50-115.946C50-143.034 27.605-165 0-165z';
      var mapStyle = [{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#d3d3d3"}]},{"featureType":"transit","stylers":[{"color":"#808080"},{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#b3b3b3"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"weight":1.8}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#d7d7d7"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ebebeb"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#a7a7a7"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#efefef"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#696969"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#737373"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#d6d6d6"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#dadada"}]}];

      var shape    = {
         coord: [9,0,6,1,4,2,2,4,0,8,0,12,1,14,2,16,5,19,7,23,8,26,9,30,9,34,11,34,11,30,12,26,13,24,14,21,16,18,18,16,20,12,20,8,18,4,16,2,15,1,13,0],
         type : 'poly'
      };

      var map;
      function initialize() {
         var mapOptions = {
            zoom: 7,
            center: new google.maps.LatLng(-28.185727, -51.394043),
            styles: mapStyle,
            streetViewControl: false,
         };
         map = new google.maps.Map(document.getElementById('mapa-google'), mapOptions);

         @foreach($marcadores as $value)
            var marker = new google.maps.Marker({
               position: new google.maps.LatLng({{ $value['latitude'] }}, {{ $value['longitude'] }}),
               title: "{{ $value['representante'] }} - {{ $value['cidade'] }}, {{ $value['uf'] }}",
               idRelacao: {{ $value['id-representante'] }},
               map: map,
               icon: {
                  path: pinMap,
                  fillColor: '{{ $value['cor'] }}',
                  fillOpacity: 0.95,
                  strokeColor: '#333',
                  strokeOpacity: 0.1,
                  strokeWeight: 2,
                  scale: 1/5
               }
            });

            google.maps.event.addListener(marker, "click", function () {
               $.fancybox({
                  maxWidth	   : 800,
            		maxHeight	: 650,
            		fitToView	: false,
            		width		   : '90%',
            		height		: '90%',
            		autoSize	   : false,
            		closeClick	: false,
            		openEffect	: 'none',
            		closeEffect	: 'none',
                  href        : '{{ URL::to("/detalhes-pin/{$value['id-relacao']}") }}',
                  type        : 'iframe'
               });

               /*var myTemplate = '<h1 class="titulo-map-pin">'+this.title+'<h1><p class="texto-map-pin"></p>';

               infowindow.setContent(myTemplate);
               infowindow.open(map, this);*/
            });
            gmarkers.push(marker);
         @endforeach

         infowindow = new google.maps.InfoWindow({
            content: "loading..."
         });
      }

      google.maps.event.addDomListener(window, 'load', initialize);
   </script>
@stop

@section('conteudo')

<!-- Header Bar -->
<div id="barra-topo-mapa" class="row header">
   <div class="col-xs-5">
      <div class="meta pull-left">
         <div class="page">
            Dashboard
         </div>
         <div class="breadcrumb-links">
            Home / Dashboard
         </div>
      </div>
   </div>
   <div class="col-xs-7" id="filtros-mapa-home">
         <div class="form-group col-xs-5">
            <label for="filtro-mapa">Filtro por Unidade:</label>
            <select class="form-control" id="filtro-mapa">
               <option value="ambas" @if(Session::get('filtro-home') == 'ambas') selected="selected" @endif>Ambas</option>
               <option value="lubrilages" @if(Session::get('filtro-home') == 'lubrilages') selected="selected" @endif>Lubrilages</option>
               <option value="novolubri" @if(Session::get('filtro-home') == 'novolubri') selected="selected" @endif>Novolubri</option>
            </select>
         </div>

         <div class="form-group col-xs-7">
            <label for="filtro-representante">Filtro por Representante:</label>
            <select class="form-control" id="filtro-representante">
               <option value="0" selected="selected">Selecione um representante para filtrar</option>
               @foreach($representantes as $key => $value)
                  <option value="{{$value->id}}" @if(Session::get('filtro-home') == $value->id) selected="selected" @endif>{{$value->nome}} {{$value->sobrenome}}</option>
               @endforeach
            </select>
         </div>
   </div>
</div>
<!-- End Header Bar -->

<div class="row" id="mapa-google"></div>

@stop
