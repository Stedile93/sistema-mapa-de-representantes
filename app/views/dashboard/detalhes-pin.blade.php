@extends('layouts.fancybox')

@section('css')
<style media="screen">
   form[name="filtros-data"] {
      margin-top: 15px;
   }

   form[name="filtros-data"] .form-control {
      height: 30px !important;
      padding: 4px 8px !important;
   }
   form[name="filtros-data"] .btn {
      padding: 4px 8px !important;
      margin-right: 10px;
   }


   #grafico-vendas {
      height: 200px;
   }
</style>
@stop


@section('javascripts')
{{ HTML::script('js/jquery.canvasjs.min.js', array('type' => 'text/javascript')) }}
<script type="text/javascript">
   $(document).ready(function(){

      var options = {
         animationEnabled: true,
         data: [
            {
               type: "spline", //change it to line, area, column, pie, etc
               dataPoints: [
                  @for ($i = 6; $i >= 1; $i--)
                     <?php
                        $stringMesAno = date("n-Y",strtotime("-{$i} Months"));
                        list($mesString, $anoString) = explode("-", $stringMesAno);

                        $vendasNoMesDoGrafico = ValorVendido::where('id_usuario', '=', $representante->id)->where('id_cidade', '=', $cidade->id)->whereMonth('data', '=', $mesString)->whereYear('data', '=', $anoString)->get()->count();

                        $nomeMes = "";
                        switch ($mesString) {
                           case 1:
                              $nomeMes = "Janeiro";
                              break;
                           case 2:
                              $nomeMes = "Fevereiro";
                              break;
                           case 3:
                              $nomeMes = "Março";
                              break;
                           case 4:
                              $nomeMes = "Abril";
                              break;
                           case 5:
                              $nomeMes = "Maio";
                              break;
                           case 6:
                              $nomeMes = "Junho";
                              break;
                           case 7:
                              $nomeMes = "Julho";
                              break;
                           case 8:
                              $nomeMes = "Agosto";
                              break;
                           case 9:
                              $nomeMes = "Setembro";
                              break;
                           case 10:
                              $nomeMes = "Outubro";
                              break;
                           case 11:
                              $nomeMes = "Novembro";
                              break;
                           case 12:
                              $nomeMes = "Dezembro";
                              break;
                        }
                     ?>
                     { label: '{{$nomeMes}}/{{$anoString}}', y: {{$vendasNoMesDoGrafico}} },
                  @endfor
               ]
            }
         ]
      };

      $("#grafico-vendas").CanvasJSChart(options);

   });
</script>
@stop

@section('conteudo')
<!-- Header Bar -->
<div class="row header">
   <div class="col-xs-7 col-md-7">
      <div class="meta pull-left">
         <div class="page">
            {{ $representante->nome }} {{ $representante->sobrenome }} - {{ $cidade->nome }} / {{ $cidade->uf }}
         </div>
         <div class="breadcrumb-links">
            Detalhes do representante
         </div>
      </div>
   </div>
   <div class="col-xs-5 col-md-5">
      <form name="filtros-data" class="form-inline meta pull-right">
         <?php
            $anosDeVendas = DB::select("SELECT YEAR(data) AS ano FROM `valores_vendidos` GROUP BY YEAR(data) ORDER BY data DESC");
            $anoAtual = date("Y");
            $mesAtual = date("n");
            $mesAtualMenos1 = $mesAtual - 1;
         ?>
         <div class="form-group">
            <label class="sr-only" for="filtro-mes">Mês:</label>
            <select class="form-control filtros-data" id="filtro-mes">
               <option value="1" @if($mesAtualMenos1 == 1) selected="selected" @endif>Janeiro</option>
               <option value="2" @if($mesAtualMenos1 == 2) selected="selected" @endif>Fevereiro</option>
               <option value="3" @if($mesAtualMenos1 == 3) selected="selected" @endif>Março</option>
               <option value="4" @if($mesAtualMenos1 == 4) selected="selected" @endif>Abril</option>
               <option value="5" @if($mesAtualMenos1 == 5) selected="selected" @endif>Maio</option>
               <option value="6" @if($mesAtualMenos1 == 6) selected="selected" @endif>Junho</option>
               <option value="7" @if($mesAtualMenos1 == 7) selected="selected" @endif>Julho</option>
               <option value="8" @if($mesAtualMenos1 == 8) selected="selected" @endif>Agosto</option>
               <option value="9" @if($mesAtualMenos1 == 9) selected="selected" @endif>Setembro</option>
               <option value="10" @if($mesAtualMenos1 == 10) selected="selected" @endif>Outubro</option>
               <option value="11" @if($mesAtualMenos1 == 11) selected="selected" @endif>Novembro</option>
               <option value="12" @if($mesAtualMenos1 == 12) selected="selected" @endif>Dezembro</option>
            </select>
         </div>
         <div class="form-group">
            <label class="sr-only" for="filtro-ano">Ano:</label>
            <select class="form-control filtros-data" id="filtro-ano">
               @foreach($anosDeVendas as $key => $value)
                  <option value="{{$value->ano}}" @if($anoAtual == $value->ano) selected="selected" @endif>{{$value->ano}}</option>
               @endforeach
            </select>
         </div>
         <button type="submit" class="btn btn-default">Filtrar</button>
      </form>
   </div>
</div>
<!-- End Header Bar -->

<div class="row paddings-conteudo">

   <div class="col-xs-4 col-md-4">
      <div class="panel panel-success">
         <div class="panel-heading">
            <div class="row">
               <div class="col-xs-3">
                  <i class="fa fa-money fa-3x"></i>
               </div>
               <div class="col-xs-9 text-right">
                  <div class="huge">{{$vendas->count()}}</div>
                  <h4>Vendas</h4>
               </div>
            </div>
         </div>
         <div class="panel-footer">
            <span class="pull-left">R$ {{ number_format($somaVendas, 2, ',', '.') }}</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-up"></i></span>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>

   <div class="col-xs-4 col-md-4">
      <div class="panel panel-info">
         <div class="panel-heading">
            <div class="row">
               <div class="col-xs-3">
                  <i class="fa fa-users fa-3x"></i>
               </div>
               <div class="col-xs-9 text-right">
                  <div class="huge">@if($cidade->habitantes == 0) Não cadastrado @else {{$cidade->habitantes}} @endif</div>
                  <h4>Habitantes</h4>
               </div>
            </div>
         </div>
         <div class="panel-footer">
            <span class="pull-left">{{ $cidade->nome }} / {{ $cidade->uf }}</span>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>

   <div class="col-xs-4 col-md-4">
      <div class="panel panel-danger">
         <div class="panel-heading">
            <div class="row">
               <div class="col-xs-3">
                  <i class="fa fa-automobile fa-3x"></i>
               </div>
               <div class="col-xs-9 text-right">
                  <div class="huge">@if($cidade->veiculos == 0) Não cadastrado @else {{$cidade->veiculos}} @endif</div>
                  <h4>Veículos</h4>
               </div>
            </div>
         </div>
         <div class="panel-footer">
            <span class="pull-left">{{ $cidade->nome }} / {{ $cidade->uf }}</span>
            <?php
            if($cidade->veiculos > 0 and $cidade->habitantes > 0){
               $veiculosPorHabitante = $cidade->veiculos / $cidade->habitantes;
               $veiculosPorHabitante = number_format($veiculosPorHabitante, 2, ",", "");

               echo "<span class='pull-right'>({$veiculosPorHabitante}/hab)</span>";
            }
            ?>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>

</div>

<div class="row paddings-conteudo">

   <h4>Vendas dos últimos 6 meses</h4>
   <div id="grafico-vendas"></div>

</div>

@stop
