<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class ConfigSistema extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'config_sistema';

}
