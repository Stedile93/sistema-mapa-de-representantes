100000 DATA      HORA
000001 2015-06-0108:00:00
200000 DATA      VENDEDOR               MUNICIPIO                      VENDIDO
000002 2015-06-01AGRO                   DONA EMMA                       20.857,20
000002 2015-06-01LUBRILAGES             LAGES                              199,28
000002 2015-06-01NOVOLUBRI              NOVO HAMBURGO                      273,68
000002 2015-06-01AGRO                   WITMARSUM                        1.800,00
000002 2015-06-01FLAVIO B               ERVAL GRANDE                     5.214,73
000002 2015-06-01FLAVIO B               FREDERICO WESTPHALEN             3.090,81
000002 2015-06-01FLAVIO B               GAURAMA                          2.164,46
000002 2015-06-01FLAVIO B               GIRUA                            1.058,83
000002 2015-06-01FLAVIO B               NONOAI                           2.980,39
000002 2015-06-01FLAVIO B               SAO JOSE DAS MISSOES             3.792,14
000002 2015-06-01FLAVIO B               SEVERIANO DE ALMEIDA             2.746,56
000002 2015-06-01FLAVIO B               TAQUARUCU DO SUL                 1.651,55
000002 2015-06-01BALCAO                 BOCAINA DO SUL                   3.499,97
000002 2015-06-01BALCAO                 CORREIA PINTO                      332,74
000002 2015-06-01BALCAO                 LAGES                            4.547,15
000002 2015-06-01BALCAO                 LUIZ ALVES                         108,41
000002 2015-06-01BALCAO                 OTACILIO COSTA                   3.373,85
000002 2015-06-01BALCAO                 SAO JOAQUIM                      1.077,83
000002 2015-06-01MUNIZ                  LAGES                            9.676,41
000002 2015-06-01OSNILDO RE             VITOR MEIRELES                     730,30
000002 2015-06-01CLOVIS                 ARARANGUA                        1.082,13
000002 2015-06-01CLOVIS                 FORQUILHINHA                     2.873,81
000002 2015-06-01CLOVIS                 ANTONIO PRADO                      795,46
000002 2015-06-01CLOVIS                 ICARA                              795,46
000002 2015-06-01CLOVIS                 SAO LEOPOLDO                       795,46
000002 2015-06-01CLOVIS                 ICARA                              795,46
000002 2015-06-01CLOVIS                 BOCAINA DO SUL                   2.058,98
000002 2015-06-01CLOVIS                 SANGAO                           1.687,99
000002 2015-06-01JOAO AP                BALNEARIO CAMBORIU                 743,02
000002 2015-06-01JOAO AP                BALNEARIO PICARRAS                 967,66
000002 2015-06-01JOAO AP                BARRA VELHA                      1.926,83
000002 2015-06-01JOAO AP                BLUMENAU                           756,46
000002 2015-06-01JOAO AP                BRUSQUE                          1.826,14
000002 2015-06-01JOAO AP                CAMBORIU                         1.582,37
000002 2015-06-01JOAO AP                ITAJAI                           2.834,26
000002 2015-06-01JOAO AP                JOINVILLE                        1.679,62
000002 2015-06-01JOAO AP                NAVEGANTES                       2.812,68
000002 2015-06-01JOAO AP                PENHA                            3.885,37
000002 2015-06-01EVERTON                BELA VISTA DO TOLDO              1.338,44
000002 2015-06-01EVERTON                CAMPOS NOVOS                     5.490,96
000002 2015-06-01EVERTON                CANOINHAS                        1.800,29
000002 2015-06-01EVERTON                CAPINZAL                         5.966,31
000002 2015-06-01EVERTON                CURITIBANOS                      3.326,48
000002 2015-06-01EVERTON                HERVAL DO OESTE                  1.122,94
000002 2015-06-01EVERTON                MAFRA                            2.268,08
000002 2015-06-01EVERTON                PAPANDUVA                          796,75
000002 2015-06-01EVERTON                PORTO UNIAO                        774,80
000002 2015-06-01ALTAIR                 ROSARIO DO SUL                   2.775,83
000002 2015-06-01ALTAIR                 SAO PEDRO DO SUL                 1.418,16
000002 2015-06-01ALEX BONDA             DOIS IRMAOS                        178,51
000002 2015-06-01ALEX BONDA             IVOTI                            2.330,38
000002 2015-06-01ALEX BONDA             PAROBE                           1.122,82
000002 2015-06-01GGS VOLMIR             CHAPECO                         18.506,10
000002 2015-06-01GGS VOLMIR             XAXIM                            2.048,61
000002 2015-06-01INATIVOS               LAGES                              779,50
000002 2015-06-01GEROMIL                ABDON BATISTA                    1.418,40
000002 2015-06-01GEROMIL                AURORA                           2.634,71
000002 2015-06-01GEROMIL                CAMPO BELO DO SUL                3.828,62
000002 2015-06-01GEROMIL                CORREIA PINTO                    6.269,53
000002 2015-06-01GEROMIL                OTACILIO COSTA                   1.206,10
000002 2015-06-01GEROMIL                SAO JOAQUIM                      1.058,28
000002 2015-06-01GEROMIL                VARGEM                             229,49
000002 2015-06-01ADRIANO                FLORIANOPOLIS                    8.636,08
000002 2015-06-01ADRIANO                LAGUNA                           1.184,32
000002 2015-06-01ADRIANO                PALHOCA                            736,48
000002 2015-06-01ADRIANO                PORTO BELO                         748,54
000002 2015-06-01ADRIANO                SAO JOAO BATISTA                 2.279,12
000002 2015-06-01ADRIANO                SAO JOSE                           884,60
000002 2015-06-01ADRIANO                TIJUCAS                            803,64
000002 2015-06-01SANDRO SIL             CAMPO BOM                          916,94
000002 2015-06-01SANDRO SIL             CIDREIRA                           648,68
000002 2015-06-01SANDRO SIL             ESTANCIA VELHA                     145,58
000002 2015-06-01SANDRO SIL             GUAIBA                           5.559,07
000002 2015-06-01SANDRO SIL             NOVO HAMBURGO                    2.985,58
000002 2015-06-01SANDRO SIL             PORTAO                           2.958,68
000002 2015-06-01SANDRO SIL             PORTO ALEGRE                       614,34
000002 2015-06-01SANDRO SIL             SAO LEOPOLDO                       266,80
000002 2015-06-01LISSANDRO              CACHOEIRINHA                       612,52
000002 2015-06-01LISSANDRO              PORTO ALEGRE                     1.104,60
000002 2015-06-01MARLON                 SANTA MARIA                      1.066,55
000002 2015-06-01OSVALDO                URUGUAIANA                       1.464,96
000002 2015-06-01RICARDO LI             MARAU                            1.411,80
000002 2015-06-01RICARDO LI             VACARIA                          2.445,63
