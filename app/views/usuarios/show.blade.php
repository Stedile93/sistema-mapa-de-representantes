@extends('layouts.master')


@section('javascripts')
@stop

@section('conteudo')

<!-- Header Bar -->
<div class="row header">
   <div class="col-xs-12">
      <div class="meta pull-left">
         <div class="page">
            {{ $usuario->nome }} {{ $usuario->sobrenome }}
         </div>
         <div class="breadcrumb-links">
            Home / Usuários / {{ $usuario->nome }} {{ $usuario->sobrenome }}
         </div>
      </div>

      <div class="meta pull-right">
         <a href="#">
            <button type="button" class="btn btn-danger">Voltar</button>
         </a>
      </div>
   </div>
</div>
<!-- End Header Bar -->


<div class="row paddings-conteudo">
   <div class="col-xs-12">

      <a href="{{ URL::to('usuarios/create') }}">
         <button type="button" class="btn btn-success">Adicionar Usuário</button>
      </a>

      <div class="jumbotron text-center">
         <h2>{{ $usuario->nome }}</h2>
         <p>
            <strong>Sobrenome:</strong> {{ $usuario->sobrenome }}<br>
            <strong>Email:</strong> {{ $usuario->email }}
         </p>
      </div>
   </div>
</div>

@stop
