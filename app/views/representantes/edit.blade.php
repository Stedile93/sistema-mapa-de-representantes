@extends('layouts.master')


@section('css')
{{ HTML::style('plugins/colorpicker/css/colpick.css', array('type' => 'text/css')) }}
<style media="screen">
   #cor {
      border-right: 35px solid #ccc;
   }
</style>
@stop

@section('javascripts')
{{ HTML::script('plugins/colorpicker/js/colpick.js', array('type' => 'text/javascript')) }}

<script type="text/javascript">
$(document).ready(function(){

   $('#cor').colpick({
      layout:'hex',
      submit:0,
      colorScheme:'dark',
      onChange:function(hsb,hex,rgb,el,bySetColor) {
         $(el).css('border-color','#'+hex);
         // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
         if(!bySetColor) $(el).val(hex);
      }
   }).keyup(function(){
      $(this).colpickSetColor(this.value);
   });

   $('#cor').colpickSetColor($('#cor').val());

   // Editar dados de Perfil
   $('form[name="form-edicao"]').on('submit', function(e){

      e.preventDefault();
      var form = $(this);

      var method = form.attr('method');
      var action = form.attr('action');

      var icone = $('button i', form);
      var botao = icone.parent();

      // Show loading
      icone.attr('class', 'fa fa-spinner fa-spin');
      botao.attr('disabled', true);

      var valores = form.serialize();
      $.ajax({
         type     : method,
         url      : action,
         data     : valores,
         dataType : 'json',
         success  : function(data){
            // Hide loading
            botao.removeAttr('disabled');
            icone.attr('class', 'fa fa-plus fa-lg');

            if(data.status == 1){
               bootbox.alert({
                  closeButton: false,
                  message : data.msg,
                  callback : function(){
                     location.href="{{ URL::to('representantes') }}";
                  }
               });
            }else{
               bootbox.alert({
                  closeButton: false,
                  message : data.msg
               });
            }
         },
         error   : function(jq,status,message){
            bootbox.alert({
               closeButton: false,
               message : "Ocorreu um erro mais complexo que o normal, contate o desenvolvedor informando a seguinte mensagem: "+ status +" - "+ message,
               callback: function(){
                  // Hide loading
                  botao.removeAttr('disabled');
                  icone.attr('class', 'fa fa-plus fa-lg');
               }
            });
         }
      });

   });


   // Relacionamento Cidade Reresentante
   $('form[name="form-cadastro-cidade"]').on('submit', function(e){

      e.preventDefault();
      var form = $(this);

      var method = form.attr('method');
      var action = form.attr('action');

      var icone = $('button i', form);
      var botao = icone.parent();

      // Show loading
      icone.attr('class', 'fa fa-spinner fa-spin');
      botao.attr('disabled', true);

      var valores = form.serialize();
      $.ajax({
         type     : method,
         url      : action,
         data     : valores,
         dataType : 'json',
         success  : function(data){
            // Hide loading
            botao.removeAttr('disabled');
            icone.attr('class', 'fa fa-plus fa-lg');

            if(data.status == 1){
               bootbox.alert({
                  closeButton: false,
                  message : data.msg,
                  callback : function(){
                     window.location.reload();
                  }
               });
            }else{
               bootbox.alert({
                  closeButton: false,
                  message : data.msg
               });
            }
         },
         error   : function(jq,status,message){
            bootbox.alert({
               closeButton: false,
               message : "Ocorreu um erro mais complexo que o normal, contate o desenvolvedor informando a seguinte mensagem: "+ status +" - "+ message,
               callback: function(){
                  // Hide loading
                  botao.removeAttr('disabled');
                  icone.attr('class', 'fa fa-plus fa-lg');
               }
            });
         }
      });

   });


   // Botão de remover relação de cidade com representante
   $('a.botao-remover').on('click', function(e){

      e.preventDefault();

      var linha = $(this).parent().parent();
      var icone = $('i', $(this));

      var idUsuario = $(this).attr('data-id');

      bootbox.confirm({
         closeButton: false,
         message : "Deseja realmente remover permanentemente esta cidade deste representante?",
         callback : function(result) {
            if(result == true){

               // Show loading
               icone.attr('class', 'fa fa-spinner fa-spin');

               $.ajax({
                  type     : 'POST',
                  url      : '/cidades-representante/'+idUsuario,
                  data     : {'_method' : 'DELETE'},
                  dataType : 'json',
                  success  : function(data){
                     // Hide loading
                     icone.attr('class', 'fa fa-remove fa-lg');

                     if(data.status == 1){
                        linha.fadeOut();
                     }else{
                        bootbox.alert({
                           closeButton: false,
                           message : data.msg
                        });
                     }
                  }
               });
            }
         }
      });

   });

});
</script>
@stop

@section('conteudo')

<!-- Header Bar -->
<div class="row header">
   <div class="col-xs-12">
      <div class="meta pull-left">
         <div class="page">
            {{ $representante->nome }} {{ $representante->sobrenome }}
         </div>
         <div class="breadcrumb-links">
            Home / Representantes / {{ $representante->nome }} {{ $representante->sobrenome }}
         </div>
      </div>
   </div>
</div>
<!-- End Header Bar -->


{{ Form::model($representante, array('route' => array('representantes.update', $representante->id), 'name' => 'form-edicao', 'method' => 'PUT')) }}

   <div class="row paddings-conteudo">

      <div class="col-md-4">
         <div class="form-group">
             {{ Form::label('nome', 'Nome') }}
             {{ Form::text('nome', null, array('class' => 'form-control', 'required')) }}
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
             {{ Form::label('sobrenome', 'Sobrenome') }}
             {{ Form::text('sobrenome', null, array('class' => 'form-control', 'required')) }}
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            {{ Form::label('email', 'Email') }}
            {{ Form::email('email', null, array('class' => 'form-control', 'required')) }}
         </div>
      </div>

   </div>


   <div class="row paddings-conteudo">

      <div class="col-md-3">
         <div class="form-group">
             {{ Form::label('login', 'Login') }}
             {{ Form::text('login', null, array('class' => 'form-control', 'autocomplete' => 'off', 'readonly')) }}
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            {{ Form::label('data-nascimento', 'Data de Nascimento') }}

            <?php
            $dataNascimento = ($representante->data_nascimento != "0000-00-00") ? Carbon::parse($representante->data_nascimento)->format('d/m/Y') : null ;
            ?>

            <div class="input-group" id="datepicker">
               {{ Form::text('data-nascimento', $dataNascimento, array('class' => 'datepicker form-control', 'placeholder' => '__/__/__', 'maxlength' => '8', 'onkeydown' => 'Mascara(this,Data);', 'onkeypress' => 'Mascara(this,Data);', 'onkeyup' => 'Mascara(this,Data);', 'required')) }}
               <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            </div>
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            {{ Form::label('cpf', 'CPF') }}
            {{ Form::text('cpf', null, array('class' => 'form-control', 'placeholder' => '___.___.___-__', 'maxlength' => '14', 'onkeydown' => 'Mascara(this,Cpf);', 'onkeypress' => 'Mascara(this,Cpf);', 'onkeyup' => 'Mascara(this,Cpf);',  'required')) }}
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            {{ Form::label('uf', 'Estado') }}
            {{ Form::select('uf', array(
                  '0' => 'UF',
                  'AC' => 'Acre',
                  'AL' => 'Alagoas',
                  'AM' => 'Amazonas',
                  'AP' => 'Amapá',
                  'BA' => 'Bahia',
                  'CE' => 'Ceará',
                  'DF' => 'Distrito Federal',
                  'ES' => 'Espírito Santo',
                  'GO' => 'Goiás',
                  'MA' => 'Maranhão',
                  'MT' => 'Mato Grosso',
                  'MS' => 'Mato Grosso do Sul',
                  'MG' => 'Minas Gerais',
                  'PA' => 'Pará',
                  'PB' => 'Paraíba',
                  'PR' => 'Paraná',
                  'PE' => 'Pernambuco',
                  'PI' => 'Piauí',
                  'RJ' => 'Rio de Janeiro',
                  'RN' => 'Rio Grande do Norte',
                  'RO' => 'Rondônia',
                  'RS' => 'Rio Grande do Sul',
                  'RR' => 'Roraima',
                  'SC' => 'Santa Catarina',
                  'SE' => 'Sergipe',
                  'SP' => 'São Paulo',
                  'TO' => 'Tocantin'
               ), null, array('class' => 'form-control', 'required')) }}
         </div>
      </div>

   </div>


   <div class="row paddings-conteudo">


      <div class="col-md-4">
         <div class="form-group">
            {{ Form::label('cidade', 'Cidade') }}
            {{ Form::text('cidade', null, array('class' => 'form-control', 'required')) }}
         </div>
      </div>

      <div class="col-md-3">

         <div class="form-group">
            {{ Form::label('cor', 'Cor do Representante') }}
            <div class="input-group">
               <?php
               $corSemHex = str_replace("#", "", $representante->cor);
               ?>
               {{ Form::text('cor', $corSemHex, array('class' => 'form-control', 'required')) }}
            </div>
         </div>
      </div>

      <div class="col-md-5">
         {{ Form::button('<i class="fa fa-plus"></i> Salvar', array('type' => 'submit', 'class' => 'btn btn-success btn-in-col pull-right margin-left-15')) }}

         <a href="{{ URL::to('usuarios') }}" class="btn btn-warning btn-in-col pull-right">
            <i class="fa fa-remove"></i> Calcelar
         </a>
      </div>

   </div>

{{ Form::close() }}

<br><hr/><br>

{{ Form::open(array('url' => 'cidades-representante', 'name' => 'form-cadastro-cidade', 'autocomplete' => 'off')) }}

   <div class="row paddings-conteudo">

      <input type="hidden" name="id-representante" value="{{ $representante->id }}">

      <div class="col-md-5">
         <div class="form-group">
            <?php
            $arrayCidadesRepresentanteNaoCadastradas = array();
            $arrayCidadesRepresentanteNaoCadastradas[0] = "Selecione";
            ?>
            @foreach($cidadesrepresentante as $key => $value)
               <?php
               $arrayCidadesRepresentanteNaoCadastradas[$value->id] = "{$value->nome} - {$value->uf}";
               ?>
            @endforeach

            {{ Form::label('cidade-representante', 'Selecione a cidade para adiciona-la à este representante') }}
            {{ Form::select('cidade-representante', $arrayCidadesRepresentanteNaoCadastradas, null, array('class' => 'form-control', 'required')) }}
         </div>
      </div>
      <div class="col-md-7">
         {{ Form::button('<i class="fa fa-plus"></i> Adicionar', array('type' => 'submit', 'class' => 'btn btn-success btn-in-col')) }}
      </div>

   </div>

{{ Form::close() }}


<div class="row paddings-conteudo">
   <div class="col-xs-12">
      <table class="table table-striped">
         <thead>
            <tr>
               <td class="coluna-remover text-center">Ação</td>
               <td>Cidade</td>
            </tr>
         </thead>
         <tbody>
            @foreach($cidades as $key => $value)
            <tr>
               <td>
                  <a class="botao-remover hover-vermelho" href="#remover" data-id="{{ $value->id_relacao }}" title="Remover">
                     Remover <i class="fa fa-remove fa-lg"></i>
                  </a>
               </td>

               <td>{{ $value->nome }} - {{ $value->uf }}</td>
            </tr>
            @endforeach
         </tbody>
      </table>
   </div>
</div>

@stop
