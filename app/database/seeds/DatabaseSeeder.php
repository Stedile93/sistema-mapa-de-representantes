<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('TabelaUsuarioSeeder');
		$this->call('TabelaCidadeSeeder');
	}

}


class TabelaUsuarioSeeder extends Seeder {

	public function run(){

		$usuarios = Usuario::get();

		if($usuarios->count() == 0){
			Usuario::create(array(
				'email' => 'stedile@ueek.com.br',
				'login' => 'stedile',
				'senha' => Hash::make('1234'),
				'nome' => 'Giuliano',
				'sobrenome' => 'Stedile',
				'tipo' => 'master'
			));
		}

		if($usuarios->count() == 1){
			Usuario::create(array(
				'email' => 'contato@ueek.com.br',
				'login' => 'contato',
				'senha' => Hash::make('1234'),
				'nome' => 'Contato',
				'sobrenome' => 'Ueek',
				'tipo' => 'admin'
			));
		}

		if($usuarios->count() == 2){
			Usuario::create(array(
				'email' => 'representante@ueek.com.br',
				'login' => 'representante',
				'senha' => Hash::make('1234'),
				'nome' => 'Representante',
				'sobrenome' => 'Ueek',
				'tipo' => 'representante'
			));
		}

		if($usuarios->count() == 3){
			Usuario::create(array(
				'email' => 'lubrilages@ueek.com.br',
				'login' => 'lubrilages',
				'senha' => Hash::make('1234'),
				'nome' => 'Lubrilages',
				'sobrenome' => 'Teste',
				'tipo' => 'admin'
			));
		}

	}

}

class TabelaCidadeSeeder extends Seeder {

	public function run(){

		$cidades = Cidade::get();

		if($cidades->count() == 0){
			Cidade::create(array(
				'nome' => 'Lages',
				'uf' => 'SC'
			));
			Cidade::create(array(
				'nome' => 'Florianópolis',
				'uf' => 'SC'
			));
			Cidade::create(array(
				'nome' => 'Porto Alegre',
				'uf' => 'RS'
			));
			Cidade::create(array(
				'nome' => 'Canoas',
				'uf' => 'RS'
			));
		}

	}

}
