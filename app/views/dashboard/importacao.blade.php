@extends('layouts.master')

@section('css')
<style media="screen">
   .barra-progresso {
      margin-top: 15px;
      margin-bottom: 40px;
      height: 20px;
   }
</style>
@stop

@section('javascripts')
<script type="text/javascript">
   $(function(){
      $('#input-file').uploadifive({
         'width'            : 180,
         'height'           : 30,
         'buttonText'       : 'Importar Dados',
         'buttonClass'      : 'btn btn-success',
         'multi'            : false,
         'auto'             : true,
         'fileTypeDesc'     : 'Text Files',
         'fileTypeExts'     : 'text/*',
         'fileSizeLimit'    : '3MB',
         'formData'         : {
            '_token' : '{{ csrf_token() }}'
         },
         'uploadScript'     : '{{ URL::to('importacao/upload') }}',
         'onUpload'         : function(filesToUpload) {
            $('#loading-import').css('display', 'block');
         },
         'onUploadComplete' : function(file, data) {
            $('#loading-import').css('display', 'none');

            var res = JSON.parse(data);

            bootbox.alert({
               closeButton: false,
               message : res.msg,
               callback: function(){
                  if(res.success == true){
                     location.href="{{ URL::to('importacao') }}";
                  }
               }
            });

         }
      });
   });
</script>
@stop

@section('conteudo')

<!-- Header Bar -->
<div class="row header">
   <div class="col-xs-12">
      <div class="meta pull-left">
         <div class="page">
            Importação de Dados
         </div>
         <div class="breadcrumb-links">
            Home / Importação de Dados
         </div>
      </div>
   </div>
</div>
<!-- End Header Bar -->

<div class="row paddings-conteudo">
   <div class="col-xs-12">

      <form class="form-upload">

         <div id="loading-import">
            <i class="fa fa-spinner fa-2x fa-spin"></i>
         </div>

         <input id="input-file" name="import" type="file">

		</form>

      <table class="table table-striped">
         <thead>
            <tr>
               <td>Data Arquivo</td>
               <td>Nome Arquivo</td>
               <td class="text-center">Registros</td>
               <td class="text-center">Registros Sucesso</td>
               <td class="text-center">Registros Duplicados</td>
               <td class="text-center">Registros Falhos</td>
               <td class="text-center">Status</td>
            </tr>
         </thead>
         <tbody>
            <?php
               $quantidadeDados = Importacao::get()->count();
            ?>
            @if ($quantidadeDados > 0)
               @foreach($importacoes as $key => $value)
                  <tr>
                     <td>{{ Carbon::parse($value->data_importacao)->format('d/m/Y H:i:s') }}</td>
                     <td>{{ $value->nome }}</td>
                     <td class="text-center">{{ $value->linhas_analisadas }}</td>
                     <td class="text-center">{{ $value->linhas_importadas }}</td>
                     <td class="text-center">{{ $value->linhas_duplicadas }}</td>
                     <td class="text-center">{{ $value->linhas_com_erro }}</td>
                     <td class="text-center">
                        @if ($value->status == 0)
                           <span class="status-vermelho">Falha</span>
                        @elseif ($value->status == 2)
                           <span class="status-azul">Processando {{ $value->porcentagem }}%</span>
                        @else
                           <span class="status-verde">Sucesso</span>
                        @endif
                     </td>
                  </tr>
               @endforeach
            @else
            <tr>
               <td class="text-center" colspan="7">Nenhum registro cadastrado até o momento!</td>
            </tr>
            @endif
         </tbody>
      </table>

      {{ $importacoes->links() }}

   </div>
</div>

@stop
