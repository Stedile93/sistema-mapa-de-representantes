<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDadosImportacao extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dados_importacao', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('id_importacao')->unsigned();
	      $table->foreign('id_importacao')->references('id')->on('importacoes');

			$table->date('data');
			$table->string('vendedor', 150);
			$table->string('cidade', 150);
			$table->decimal('valor_vendido', 11, 2);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dados_importacao');
	}

}
