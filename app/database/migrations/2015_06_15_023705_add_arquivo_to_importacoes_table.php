<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArquivoToImportacoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('importacoes', function(Blueprint $table)
		{
			$table->string('arquivo', 255);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('importacoes', function(Blueprint $table)
		{
			$table->dropColumn('arquivo');
		});
	}

}
