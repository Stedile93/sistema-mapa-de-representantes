<?php

ignore_user_abort(true);
set_time_limit(0);

require 'database.php';

include 'models/Cidade.php';
include 'models/CidadeDoBrasil.php';
include 'models/CidadeRepresentante.php';
include 'models/DadosImportacao.php';
include 'models/Importacao.php';
include 'models/Notificacao.php';
include 'models/Usuario.php';
include 'models/ValorVendido.php';
include 'models/ConfigSistema.php';

include 'funcoes.php';

use Carbon\Carbon;


if ($argc < 2) {
   //Sem informações necessária para o processamento.
   exit;
}

$idImportacao = (int) $argv[1];

if ($idImportacao > 0) {

   $importacao = Importacao::find($idImportacao);

   if ($importacao->count()) {

      $dadosImportacao = DadosImportacao::where('id_importacao', '=', $importacao->id)->get();

      echo "antes do foreach\n\n";

      $totalDados = $dadosImportacao->count();

      $count = 0;
      foreach ($dadosImportacao as $key => $value) {

         $idRepresentante = cadastrarRepresentante($value->vendedor);
         $idCidade        = cadastrarCidade($value->cidade);
         $idValorVendido  = cadastrarValorVendido($idRepresentante, $idCidade, $value->data, $value->valor_vendido);

         if ($idRepresentante and $idCidade) {
            cadastrarCidadeRepresentante($idCidade, $idRepresentante);
         }

         echo "#{$count} - dentro do foreach\n";

         $count++;

         $porcentagem = number_format( (($count*100)/$totalDados), 2, '.', '' );

         $importacaoPorcentagem = Importacao::find($idImportacao);
         $importacaoPorcentagem->porcentagem = $porcentagem;
         $importacaoPorcentagem->save();

         sleep(8);

      }

      echo "\ndepois do foreach";

      $importacaoSucesso = Importacao::find($idImportacao);
      $importacaoSucesso->status = 1;
      $importacaoSucesso->save();

      $notificacao = new Notificacao();
      $notificacao->usuario_id = $importacao->id_usuario;
      $notificacao->tipo = "notificacao";
      $notificacao->assunto = "Importação do arquivo {$importacao->nome} finalizada!";
      $notificacao->texto = "A importação do arquivo {$importacao->nome} foi finalizada com sucesso! Os dados importados já estão disponíveis para visualização e análise.";
      $notificacao->enviada_em = Carbon::now();
      $notificacao->remetente_id = 0;
      $notificacao->save();

      exit;

   }

} else {
   exit;
}
