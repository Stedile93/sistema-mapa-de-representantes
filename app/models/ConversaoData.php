<?php

class ConversaoData {

	public static function textToSql($data)
	{
		list($d, $m, $a) = explode("/", $data);
		return "$a-$m-$d";
	}

	public static function SqlToText($data, $dateTime = false)
	{
		list($data, $hora) = explode(" ", $data);

		if($dateTime == true){
			$hora = explode(":", $hora);
			$hora = "{$hora[0]}:{$hora[1]}";
		}

		list($a, $m, $d) = explode("-", $data);

		return ($dateTime == true) ? "$d/$m/$a &agrave;s {$hora}" : "$d/$m/$a";
	}

}
