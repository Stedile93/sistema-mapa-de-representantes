<?php

class HomeController extends BaseController {


	public function home(){

		/*
		$sender = Usuario::where('nome', '=', 'Giuliano')->first();
		$user   = Auth::user();

		$user->newNotification()
			->from($sender)
			->withType('notificacao')
			->withSubject('Teste de notificação =)')
			->withBody('Texto de teste de notificação')
			->deliver();
		*/

		if(Auth::user()->tipo == "representante"){
			$cidadesRepresentante = CidadeRepresentante::where('id_usuario', '=', Auth::user()->id)->get();
		}else{
			$cidadesRepresentante = CidadeRepresentante::all();
		}

		$representantes = Usuario::where('tipo', '=', 'representante')->orderBy('nome', 'asc')->get();

		$marcadoresGoogle = array();
		foreach($cidadesRepresentante as $key => $value){
			$usuarioCR = Usuario::find($value->id_usuario);
			$cidadeCR  = Cidade::find($value->id_cidade);

			if($cidadeCR->status == 1){
				$marcadoresGoogle[] = array(
					'id-relacao'       => $value->id,
					'id-representante' => $usuarioCR->id,
					'representante'    => $usuarioCR->nome,
					'cor'              => $usuarioCR->cor,
					'id-cidade'        => $cidadeCR->id,
					'cidade'           => $cidadeCR->nome,
					'uf'               => $cidadeCR->uf,
					'latitude'         => $cidadeCR->latitude,
					'longitude'        => $cidadeCR->longitude
				);
			}
		}

		$dados = array(
			'marcadores' => $marcadoresGoogle,
			'representantes' => $representantes
		);

		return View::make('dashboard.home')->with($dados);
	}

	public function detalhesPIN($id){

		$anoAtual = date("Y");
		$mesAtual = date("n");
		$mesAtualMenos1 = $mesAtual - 1;

		$relacaoRepresentanteCidade = CidadeRepresentante::find($id);

		$representante = Usuario::find($relacaoRepresentanteCidade->id_usuario);
		$cidade = Cidade::find($relacaoRepresentanteCidade->id_cidade);

		$vendas     = ValorVendido::where('id_usuario', '=', $representante->id)->where('id_cidade', '=', $cidade->id)->whereMonth('data', '=', $mesAtualMenos1)->whereYear('data', '=', $anoAtual)->orderBy('data', 'desc')->get();
		$somaVendas = ValorVendido::where('id_usuario', '=', $representante->id)->where('id_cidade', '=', $cidade->id)->whereMonth('data', '=', $mesAtualMenos1)->whereYear('data', '=', $anoAtual)->orderBy('data', 'desc')->sum('valor_vendido');

		return View::make('dashboard.detalhes-pin')->with(
			array(
				'representante' => $representante,
				'cidade' => $cidade,
				'vendas' => $vendas,
				'somaVendas' => $somaVendas
			)
		);
	}

	public function perfil(){
		$usuario = Usuario::find(Auth::user()->id);
		return View::make('dashboard.perfil-e-configuracoes')->with( array(
			'usuario' => $usuario
		));
	}


	public function mensagensNotificacoes(){
		$notificacoes = Auth::user()->notifications()->orderBy('created_at', 'desc')->paginate(10);
		return View::make('dashboard.mensagens-notificacoes')->with( array(
			'notificacoes' => $notificacoes
		));
	}

	public function getConteudoNotificacao($id){
		$notificacao = Notificacao::find($id);

		$notificacao->lida = 1;

		$notificacao->save();

		return Response::json([
			'data' => $notificacao->created_at,
			'assunto' => $notificacao->assunto,
			'texto' => $notificacao->texto
			]);
	}


	public function vendas(){
		$vendas = ValorVendido::orderBy('created_at', 'desc')->paginate(10);
		return View::make('dashboard.vendas')->with( array(
			'vendas' => $vendas
		));
	}

	public function relatorios(){
		$vendas = ValorVendido::orderBy('created_at', 'desc')->paginate(10);
		return View::make('dashboard.relatorios-estatisticas')->with( array(
			'vendas' => $vendas
		));
	}


	public function getIndex(){
		if(Auth::check()){
			return Redirect::to('home');
		}else{
			return Redirect::to('entrar');
		}
	}

	public function getEntrar(){
		if(Auth::check()){
			return Redirect::to('home');
		}

		$titleHtml = "Entrar - Controle de Estatísticas Lubrilages";
		return View::make('login', compact('tituloHtml'));
	}

	public function postEntrar(){

		$remember = false;
		if(Input::get('remember')){
			$remember = true;
		}

		// Autenticação
		if(Auth::attempt(array('login' => Input::get('login'), 'password' => Input::get('senha')), $remember)){
			return Redirect::to('home');
		}else{
			return Redirect::to('entrar')->with('flash_error', 1)->withInput();
		}

	}


	public function getSair(){
		Auth::logout();
		return Redirect::to('/');
	}

}
