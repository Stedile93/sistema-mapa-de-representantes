<?php

class CidadesController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($termoBuscado = null)
	{
		if(Auth::user()->tipo == "representante")
			return Redirect::to('/');

		if($termoBuscado <> null){
         $cidades = Cidade::where('nome', 'LIKE', '%'.$termoBuscado.'%')->orderBy('nome', 'asc')->paginate(15);
      }else{
         $cidades = Cidade::orderBy('nome', 'asc')->paginate(15);
      }

      $dados = array(
         'cidades' => $cidades,
         'termoBuscado' => $termoBuscado
      );

		return View::make('cidades.index')->with($dados);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if(Auth::user()->tipo == "representante")
			return Redirect::to('/');

		return View::make('cidades.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if(empty(Input::get('nome')) or Input::get('uf') == "0"){
			return Response::json(['status' => 0, 'msg' => 'Todos os dados precisam ser preenchidos!']);
		}

		$dadosGoogle = Cidade::pegaLocationCidade(Input::get('nome'));

		if(Cidade::where('nome' , '=', $dadosGoogle['nome-cidade'])->count() == 1){
			return Response::json(['status' => 0, 'msg' => 'Esta cidade já está cadastrada!']);
		}

		$cidade = new Cidade();
      $cidade->nome = Input::get('nome');
      $cidade->uf   = Input::get('uf');

		if($dadosGoogle){
	      $cidade->nome      = $dadosGoogle['nome-cidade'];
		   $cidade->latitude  = $dadosGoogle['latitude'];
			$cidade->longitude = $dadosGoogle['longitude'];
			$cidade->status    = 1;
		}

		$habitantesEVeiculos = CidadeDoBrasil::where('uf', '=', $cidade->uf)->where('cidade', 'LIKE', '%'.$cidade->nome.'%')->first();

		if($habitantesEVeiculos->populacao > 0){
			$cidade->habitantes = $habitantesEVeiculos->populacao;
		}
		if($habitantesEVeiculos->frota_veiculos > 0){
			$cidade->veiculos = $habitantesEVeiculos->frota_veiculos;
		}

		if($cidade->save()){
			return Response::json(['status' => 1, 'msg' => 'Cidade cadastrada com sucesso!']);
		}else{
			return Response::json(['status' => 0, 'msg' => 'Ocorreu algum erro ao cadastrar esta cidade!']);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if(Auth::user()->tipo == "representante")
			return Redirect::to('/');

		$cidade = Cidade::find($id);
		return View::make('cidades.show')->with('cidade', $cidade);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if(Auth::user()->tipo == "representante")
			return Redirect::to('/');

		$cidade = Cidade::find($id);
		return View::make('cidades.edit')->with('cidade', $cidade);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(empty(Input::get('nome')) or Input::get('uf') == "0"){
			return Response::json(['status' => 0, 'msg' => 'Todos os dados precisam ser preenchidos!']);
		}

		$dadosGoogle = Cidade::pegaLocationCidade(Input::get('nome'));

		if(Cidade::where('nome' , '=', $dadosGoogle['nome-cidade'])->where('id', '<>', $id)->count() == 1){
			return Response::json(['status' => 0, 'msg' => 'Esta cidade já está cadastrada!']);
		}

		$cidade = Cidade::find($id);
      $cidade->nome = Input::get('nome');
      $cidade->uf   = Input::get('uf');


		if($dadosGoogle){
	      $cidade->nome      = $dadosGoogle['nome-cidade'];
		   $cidade->latitude  = $dadosGoogle['latitude'];
			$cidade->longitude = $dadosGoogle['longitude'];
			$cidade->status    = 1;
		}

		$habitantesEVeiculos = CidadeDoBrasil::where('uf', '=', $cidade->uf)->where('cidade', 'LIKE', '%'.$cidade->nome.'%')->first();

		$habitantesForm = (int) Input::get('habitantes');
		$veiculosForm   = (int) Input::get('veiculos');

		$cidadeDoBrazil = CidadeDoBrasil::find($habitantesEVeiculos->id);

		if($habitantesForm < $habitantesEVeiculos->populacao){
			$cidade->habitantes = $habitantesEVeiculos->populacao;
		}else{
			$cidade->habitantes = $habitantesForm;
			$cidadeDoBrazil->populacao = $habitantesForm;
		}

		if($veiculosForm < $habitantesEVeiculos->frota_veiculos){
			$cidade->veiculos = $habitantesEVeiculos->frota_veiculos;
		}else{
			$cidade->veiculos = $veiculosForm;
			$cidadeDoBrazil->frota_veiculos = $veiculosForm;
		}

		$cidadeDoBrazil->save();

		if($cidade->save()){
			return Response::json(['status' => 1, 'msg' => 'Cidade alterada com sucesso!']);
		}else{
			return Response::json(['status' => 0, 'msg' => 'Ocorreu algum erro ao alterar esta cidade!']);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/*public function destroy($id)
	{
		$cidade = Cidade::find($id);
		$cidade->delete();

		Session::flash('message', 'Cidade removida com sucesso!');
		return Redirect::to('/cidades');
	}*/


}
