<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCidadesDoBrasil extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cidades_do_brasil', function($table){

			$table->increments('id');

			$table->integer('ibge');

			$table->string('cidade', 200);
			$table->char('uf', 2);

			$table->integer('populacao');
			$table->integer('frota_veiculos');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cidades_do_brasil');
	}

}
