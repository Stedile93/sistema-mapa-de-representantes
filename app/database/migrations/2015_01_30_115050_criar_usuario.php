<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarUsuario extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuarios', function($table){

			$table->increments('id');

			$table->string('email', 255);
			$table->string('login', 30)->unique();
			$table->char('senha', 64);
			$table->string('nome', 150);
			$table->string('sobrenome', 200);
			$table->date('data_nascimento');
			$table->string('cpf', 14);
			$table->char('uf', 2);
			$table->string('cidade', 150);

			$table->string('avatar', 255);
			$table->string('cor', 7)->default('#999999');

			$table->enum('tipo', array('master', 'admin', 'representante'));
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
			$table->enum('ativo', array('0', '1'))->default(1);

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usuarios');
	}

}
