@extends('layouts.master')


@section('javascripts')
@stop

@section('css')
<style media="screen">
   /** Invoice **/
   .grid.simple {
      background-color: #fff;
      -webkit-box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.75);
-moz-box-shadow:    0px 0px 5px 0px rgba(0, 0, 0, 0.75);
box-shadow:         0px 0px 5px 0px rgba(0, 0, 0, 0.75);
   }

   .grid.simple .grid-body.invoice-body{
      padding:30px;
   }

   .grid.simple .grid-body.invoice-body .invoice-logo{
      margin-bottom:18px;
   }
   .invoice-button-action-set{
      position:fixed;
   }
</style>

{{ HTML::style('css/print.css', array('type' => 'text/css')) }}
@stop

@section('conteudo')

<!-- Header Bar -->
<div class="row header">
   <div class="col-xs-12">
      <div class="meta pull-left">
         <div class="page">
            Relatórios e Estatísticas
         </div>
         <div class="breadcrumb-links">
            Home / Relatórios e Estatísticas
         </div>
      </div>
   </div>
</div>
<!-- End Header Bar -->

<div class="row paddings-conteudo">
   <div class="col-md-11">

      <div class="grid simple">
         <div class="grid-body no-border invoice-body">
            <br>
            <div class="pull-left">
               <img src="/img/logo.png" data-src="/img/logo.png" data-src-retina="/img/logo.png" class="invoice-logo">
               <address>
                  <br>
                  BR 282 - Km 212, nº 6262<br>
                  Bairro Gethal, CEP 88520-408<br>
                  Lages, SC<br>
                  Fone: 0800 704 8866
               </address>
            </div>
            <div class="pull-right">
               <h2>Relatório de Representantes<br />do Ano de 2015</h2>
            </div>
            <div class="clearfix"></div>
            <br>
            <br>
            <br>
            <div class="row">
               <div class="col-md-3">
                  <br>
                  <div>
                     <div class="pull-left"> DATA DO RELATÓRIO: </div>
                     <div class="pull-right"> {{ date('d/m/Y') }} </div>
                     <div class="clearfix"></div>
                  </div>
                  <br>
                  <div class="well well-small green">
                     <div class="pull-left"> Total de Cadastrados</div>
                     <div class="pull-right"> 192384 </div>
                     <div class="clearfix"></div>
                  </div>
               </div>
            </div>
            <br />
            <table class="table">
               <thead>
                  <tr>
                     <th width="200" class="unseen text-center">ANO</th>
                     <th width="200" class="text-center">MÊS</th>
                     <th width="200" class="text-center">QTD. NOVOS CONSUMIDORES</th>
                     <th width="200" class="text-center">QTD. TOTAL DE CONSUMIDORES</th>
                     <th width="200" class="text-center">VALOR MOVIMENTADO</th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td class="unseen text-center">2014</td>
                     <td class="text-center">Janeiro</td>
                     <td class="text-center">100</td>
                     <td class="text-center">200</td>
                     <td class="text-center">R$500,00</td>
                  </tr>
                  <tr>
                     <td class="unseen text-center">2014</td>
                     <td class="text-center">Fevereiro</td>
                     <td class="text-center">100</td>
                     <td class="text-center">200</td>
                     <td class="text-center">R$500,00</td>
                  </tr>
                  <tr>
                     <td class="unseen text-center">2014</td>
                     <td class="text-center">Março</td>
                     <td class="text-center">100</td>
                     <td class="text-center">200</td>
                     <td class="text-center">R$500,00</td>
                  </tr>
                  <tr>
                     <td class="unseen text-center">2014</td>
                     <td class="text-center">Abril</td>
                     <td class="text-center">100</td>
                     <td class="text-center">200</td>
                     <td class="text-center">R$500,00</td>
                  </tr>
                  <tr>
                     <td class="unseen text-center">2014</td>
                     <td class="text-center">Maio</td>
                     <td class="text-center">100</td>
                     <td class="text-center">200</td>
                     <td class="text-center">R$500,00</td>
                  </tr>
                  <tr>
                     <td class="unseen text-center">2014</td>
                     <td class="text-center">Junho</td>
                     <td class="text-center">100</td>
                     <td class="text-center">200</td>
                     <td class="text-center">R$500,00</td>
                  </tr>
                  <tr>
                     <td class="unseen text-center">2014</td>
                     <td class="text-center">Julho</td>
                     <td class="text-center">100</td>
                     <td class="text-center">200</td>
                     <td class="text-center">R$500,00</td>
                  </tr>
                  <tr>
                     <td class="unseen text-center">2014</td>
                     <td class="text-center">Agosto</td>
                     <td class="text-center">100</td>
                     <td class="text-center">200</td>
                     <td class="text-center">R$500,00</td>
                  </tr>
                  <tr>
                     <td class="unseen text-center">2014</td>
                     <td class="text-center">Setembro</td>
                     <td class="text-center">100</td>
                     <td class="text-center">200</td>
                     <td class="text-center">R$500,00</td>
                  </tr>
                  <tr>
                     <td class="unseen text-center">2014</td>
                     <td class="text-center">Outubro</td>
                     <td class="text-center">100</td>
                     <td class="text-center">200</td>
                     <td class="text-center">R$500,00</td>
                  </tr>
                  <tr>
                     <td class="unseen text-center">2014</td>
                     <td class="text-center">Novembro</td>
                     <td class="text-center">100</td>
                     <td class="text-center">200</td>
                     <td class="text-center">R$500,00</td>
                  </tr>
                  <tr>
                     <td class="unseen text-center">2014</td>
                     <td class="text-center">Dezembro</td>
                     <td class="text-center">100</td>
                     <td class="text-center">200</td>
                     <td class="text-center">R$500,00</td>
                  </tr>
                  <tr>
                     <td colspan="2" rowspan="4" >
                        <h4 class="semi-bold">Sobre os itens aqui relacionados</h4>
                        <p>Os números aqui relacionados dizem respeito a quantidade de cadastros de consumidores no ano de 2014, sendo este para fins de conferências e histórico.</p>
                     </td>
                     <td class="text-center"><strong>Subtotal</strong></td>
                     <td class="text-center">900 consumidores</td>
                     <td class="text-center">R$4000,00</td>
                  </tr>
                  <tr>
                     <td class="text-center no-border">
                        <div class="well well-small green"><strong>Total</strong></div>
                     </td>
                     <td class="text-center"><strong>900 consumidores</strong></td>
                  </tr>
               </tbody>
            </table>
            <br>
            <br>
            <h5 class="text-right text-black">Sistema Lubrilages</h5>
            <h5 class="text-right semi-bold text-black">Métricas e Estatísticas</h5>
            <br>
            <br>
         </div>
      </div>

   </div>

   <div class="col-md-1">
      <div class="invoice-button-action-set">
         <p>
            <button class="btn btn-primary" type="button" onclick="window.print();">
               <i class="fa fa-print fa-lg"></i>
            </button>
         </p>
      </div>
   </div>
</div>

@stop
