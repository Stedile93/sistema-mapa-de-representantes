@extends('layouts.master')


@section('javascripts')
<script type="text/javascript">
$(document).ready(function(){

   // Editar dados de Perfil
   $('form[name="form-edicao"]').on('submit', function(e){

      e.preventDefault();
      var form = $(this);

      var method = form.attr('method');
      var action = form.attr('action');

      var icone = $('button i', form);
      var botao = icone.parent();




      // Show loading
      icone.attr('class', 'fa fa-spinner fa-spin');
      botao.attr('disabled', true);

      var valores = form.serialize();
      $.ajax({
         type     : method,
         url      : action,
         data     : valores,
         dataType : 'json',
         success  : function(data){
            // Hide loading
            botao.removeAttr('disabled');
            icone.attr('class', 'fa fa-plus fa-lg');

            if(data.status == 1){
               bootbox.alert({
                  closeButton: false,
                  message : data.msg,
                  callback : function(){
                     location.href="{{ URL::to('usuarios') }}";
                  }
               });
            }else{
               bootbox.alert({
                  closeButton: false,
                  message : data.msg
               });
            }
         },
         error   : function(jq,status,message){
            bootbox.alert({
               closeButton: false,
               message : "Ocorreu um erro mais complexo que o normal, contate o desenvolvedor informando a seguinte mensagem: "+ status +" - "+ message,
               callback: function(){
                  // Hide loading
                  botao.removeAttr('disabled');
                  icone.attr('class', 'fa fa-plus fa-lg');
               }
            });
         }
      });

   });

});
</script>
@stop

@section('conteudo')

<!-- Header Bar -->
<div class="row header">
   <div class="col-xs-12">
      <div class="meta pull-left">
         <div class="page">
            {{ $usuario->nome }} {{ $usuario->sobrenome }}
         </div>
         <div class="breadcrumb-links">
            Home / Usuários / {{ $usuario->nome }} {{ $usuario->sobrenome }}
         </div>
      </div>
   </div>
</div>
<!-- End Header Bar -->


{{ Form::model($usuario, array('route' => array('usuarios.update', $usuario->id), 'name' => 'form-edicao', 'method' => 'PUT')) }}

   <div class="row paddings-conteudo">

      <div class="col-md-4">
         <div class="form-group">
             {{ Form::label('nome', 'Nome') }}
             {{ Form::text('nome', null, array('class' => 'form-control', 'required')) }}
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
             {{ Form::label('sobrenome', 'Sobrenome') }}
             {{ Form::text('sobrenome', null, array('class' => 'form-control', 'required')) }}
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            {{ Form::label('email', 'Email') }}
            {{ Form::email('email', null, array('class' => 'form-control', 'required')) }}
         </div>
      </div>

   </div>


   <div class="row paddings-conteudo">

      <div class="col-md-3">
         <div class="form-group">
             {{ Form::label('login', 'Login') }}
             {{ Form::text('login', null, array('class' => 'form-control', 'autocomplete' => 'off', 'readonly')) }}
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            {{ Form::label('data-nascimento', 'Data de Nascimento') }}

            <?php
            $dataNascimento = ($usuario->data_nascimento != "0000-00-00") ? Carbon::parse($usuario->data_nascimento)->format('d/m/Y') : null ;
            ?>

            <div class="input-group" id="datepicker">
               {{ Form::text('data-nascimento', $dataNascimento, array('class' => 'datepicker form-control', 'placeholder' => '__/__/__', 'maxlength' => '8', 'onkeydown' => 'Mascara(this,Data);', 'onkeypress' => 'Mascara(this,Data);', 'onkeyup' => 'Mascara(this,Data);', 'required')) }}
               <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            </div>
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            {{ Form::label('cpf', 'CPF') }}
            {{ Form::text('cpf', null, array('class' => 'form-control', 'placeholder' => '___.___.___-__', 'maxlength' => '14', 'onkeydown' => 'Mascara(this,Cpf);', 'onkeypress' => 'Mascara(this,Cpf);', 'onkeyup' => 'Mascara(this,Cpf);',  'required')) }}
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            {{ Form::label('uf', 'Estado') }}
            {{ Form::select('uf', array(
                  '0' => 'UF',
                  'AC' => 'Acre',
                  'AL' => 'Alagoas',
                  'AM' => 'Amazonas',
                  'AP' => 'Amapá',
                  'BA' => 'Bahia',
                  'CE' => 'Ceará',
                  'DF' => 'Distrito Federal',
                  'ES' => 'Espírito Santo',
                  'GO' => 'Goiás',
                  'MA' => 'Maranhão',
                  'MT' => 'Mato Grosso',
                  'MS' => 'Mato Grosso do Sul',
                  'MG' => 'Minas Gerais',
                  'PA' => 'Pará',
                  'PB' => 'Paraíba',
                  'PR' => 'Paraná',
                  'PE' => 'Pernambuco',
                  'PI' => 'Piauí',
                  'RJ' => 'Rio de Janeiro',
                  'RN' => 'Rio Grande do Norte',
                  'RO' => 'Rondônia',
                  'RS' => 'Rio Grande do Sul',
                  'RR' => 'Roraima',
                  'SC' => 'Santa Catarina',
                  'SE' => 'Sergipe',
                  'SP' => 'São Paulo',
                  'TO' => 'Tocantin'
               ), null, array('class' => 'form-control', 'required')) }}
         </div>
      </div>

   </div>


   <div class="row paddings-conteudo">


      <div class="col-md-4">
         <div class="form-group">
            {{ Form::label('cidade', 'Cidade') }}
            {{ Form::text('cidade', null, array('class' => 'form-control', 'required')) }}
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            {{ Form::label('tipo', 'Tipo de Usuário') }}
            {{ Form::select('tipo', array(
                  '0'             => 'Selecione o Tipo de Usuário',
                  'master'        => 'Admin Master',
                  'admin'         => 'Administrador'
               ), null, array('class' => 'form-control', 'required')) }}
         </div>
      </div>
      <div class="col-md-4">
         {{ Form::button('<i class="fa fa-plus"></i> Salvar', array('type' => 'submit', 'class' => 'btn btn-success btn-in-col pull-right margin-left-15')) }}

         <a href="{{ URL::to('usuarios') }}" class="btn btn-warning btn-in-col pull-right">
            <i class="fa fa-remove"></i> Calcelar
         </a>
      </div>

   </div>

{{ Form::close() }}

@stop
