<!doctype html>
<html lang="pt-BR">

<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <meta name="description" content="Sistema de Estatísticas e Relatórios Lubrilages">
   <meta name="author" content="Ueek Agência Digital">

   <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

   <title>Sistema - LUBRILAGES E NOVOLUBRI COM. LUBRIFICANTE</title>

   <!-- STYLES -->
   {{ HTML::style('css/bootstrap.min.css', array('type' => 'text/css')) }} {{ HTML::style('css/font-awesome.min.css', array('type' => 'text/css')) }} {{ HTML::style('css/login.css', array('type' => 'text/css')) }}

   <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <![endif]-->
</head>

<body>

   <div class="container" id="login-block">
      <div class="row">
         <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
            <div class="page-icon-shadow animated bounceInDown"></div>

            <div class="login-box clearfix animated flipInY">
               <div class="page-icon animated bounceInDown">
                  <img src="img/logo-simples.png" alt="Sistema Lubrilages">
               </div>

               <h2 class="nome-sistema">
                  Sistema Lubrilages
               </h2>

               <hr />

               <div class="login-form">
                  <!-- Start Error box -->
                  @if (Session::has('flash_error'))
                  <div class="alert alert-danger">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <h4>Ops!</h4>
                     Login ou Senha inválidos
                  </div>
                  @endif
                  <!-- End Error box -->

                  {{ Form::open(array('url' => 'entrar')) }}

                     {{ Form::text('login', '', array('class' => 'input-field', 'placeholder' => 'Login', 'required', 'autofocus')) }}

                     {{ Form::password('senha', array('class' => 'input-field', 'placeholder' => 'Senha', 'required')) }}

                     <label id="label-lembrar" for="checkLembrar">
                        {{ Form::checkbox('remember', 'remember', true, array('id' => 'checkLembrar', 'class' => 'checkbox')) }} Lembrar-me
                     </label>

                     {{ Form::submit('Entrar', array('class' => 'btn btn-login')) }}

                  {{ Form::close() }}

                  <div class="login-links">
                     <a href="forgot-password.html">Esqueceu sua senha?</a>
                  </div>
               </div>
            </div>

         </div>
      </div>
   </div>

   <!-- End Login box -->
   <footer class="container">
      <p id="footer-text">
         <small>Copyright &copy; 2015 - Todos os Direitos Reservados</small>
      </p>
   </footer>

   <!-- SCRIPTS -->
   {{ HTML::script('js/jquery.min.js', array('type' => 'text/javascript')) }}
   {{ HTML::script('js/bootstrap.min.js', array('type' => 'text/javascript')) }}
</body>

</html>
