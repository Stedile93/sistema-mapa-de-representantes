<?php

class RepresentantesController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($termoBuscado = null)
	{
		if(Auth::user()->tipo == "representante")
			return Redirect::to('/');

		if($termoBuscado <> null){
         $representantes = Usuario::where('tipo', '=', 'representante')->where('nome', 'LIKE', '%'.$termoBuscado.'%')->orderBy('nome', 'asc')->paginate(15);
      }else{
         $representantes = Usuario::where('tipo', '=', 'representante')->orderBy('nome', 'asc')->paginate(15);
      }

      $dados = array(
         'representantes' => $representantes,
         'termoBuscado' => $termoBuscado
      );


		return View::make('representantes.index')->with($dados);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if(Auth::user()->tipo == "representante")
			return Redirect::to('/');

		return View::make('representantes.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if(empty(Input::get('nome')) or empty(Input::get('sobrenome')) or empty(Input::get('email')) or empty(Input::get('login')) or empty(Input::get('data-nascimento')) or empty(Input::get('cpf')) or empty(Input::get('cidade'))){
			return Response::json(['status' => 0, 'msg' => 'Todos os dados precisam ser preenchidos!']);
		}

		$countLogin = Usuario::where('login', '=', Input::get('login'))->get()->count();

		if($countLogin > 0){
			return Response::json(['status' => 0, 'msg' => 'Este login já está sendo usado. Tente outro!']);
		}

		$representante = new Usuario();
      $representante->nome      = Input::get('nome');
      $representante->sobrenome = Input::get('sobrenome');
      $representante->login     = Input::get('login');
      $representante->email     = Input::get('email');

		if( !empty(Input::get('data-nascimento')) ){
			$representante->data_nascimento = ConversaoData::textToSql(Input::get('data-nascimento'));
		}

      $representante->cpf       = Input::get('cpf');

		if(Input::get('uf') == '0'){
			return Response::json(['status' => 0, 'msg' => 'Selecione um estado!']);
		}

		$representante->uf        = Input::get('uf');
		$representante->cidade    = Input::get('cidade');

      $representante->cor       = Usuario::randomColor();
		$representante->tipo      = "representante";

		if($representante->save()){
			return Response::json(['status' => 1, 'msg' => 'Representante cadastrado com sucesso!']);
		}else{
			return Response::json(['status' => 0, 'msg' => 'Ocorreu algum erro ao cadastrar este representante!']);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if(Auth::user()->tipo == "representante")
			return Redirect::to('/');

		$representante = Usuario::find($id);
		return View::make('representantes.show')->with('representante', $representante);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if(Auth::user()->tipo == "representante")
			return Redirect::to('/');

		$representante        = Usuario::find($id);
		$cidadesRepresentante = DB::select("SELECT cr.id as id_relacao, c.id, c.nome, c.uf FROM cidades c, cidades_representante cr WHERE cr.id_usuario = {$id} AND c.id = cr.id_cidade GROUP BY c.id ORDER BY c.nome ASC");

		$cidadesRepresentandeNaoCadastrado = DB::select("SELECT c.id, c.nome, c.uf FROM cidades c, cidades_representante cr WHERE cr.id_usuario = {$id} AND c.id <> cr.id_cidade GROUP BY c.id ORDER BY c.nome ASC");

		return View::make('representantes.edit')->with(array('representante' => $representante, 'cidades' => $cidadesRepresentante, 'cidadesrepresentante' => $cidadesRepresentandeNaoCadastrado));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(empty(Input::get('nome')) or empty(Input::get('sobrenome')) or empty(Input::get('email')) or empty(Input::get('data-nascimento')) or empty(Input::get('cpf')) or empty(Input::get('cidade'))){
			return Response::json(['status' => 0, 'msg' => 'Todos os dados precisam ser preenchidos!']);
		}

		$representante = Usuario::find($id);

		$representante->nome      = Input::get('nome');
		$representante->sobrenome = Input::get('sobrenome');
		$representante->email     = Input::get('email');

		if( !empty(Input::get('data-nascimento')) ){
			$representante->data_nascimento = ConversaoData::textToSql(Input::get('data-nascimento'));
		}
		$representante->cpf = Input::get('cpf');

		if(Input::get('uf') == '0'){
			return Response::json(['status' => 0, 'msg' => 'Selecione um estado!']);
		}

		$representante->uf     = Input::get('uf');
		$representante->cidade = Input::get('cidade');


		$representante->cor = "#".Input::get('cor');

		if($representante->save()){
			return Response::json(['status' => 1, 'msg' => "Dados de representante alterado com sucesso!"]);
		}else{
			return Response::json(['status' => 0, 'msg' => "Ocorreu algum erro ao alterar este representante!"]);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/*public function destroy($id)
	{
		$representante = Usuario::find($id);
		$representante->delete();

		Session::flash('message', 'Representante removido com sucesso!');
		return Redirect::to('/representantes');
	}*/


}
