<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::when('*', 'csrf', array('post'));

Route::get('/',
	array(
		'as' => 'home',
		'uses' => 'HomeController@getIndex'
	)
);

Route::get('entrar', 'HomeController@getEntrar');
Route::post('entrar', 'HomeController@postEntrar');
Route::get('sair', 'HomeController@getSair');


Route::group(array('before' => 'auth'), function(){

	// Rotas comuns a todos os usuários
	Route::get('home', 'HomeController@home');
	Route::get('vendas', 'HomeController@vendas');
	Route::get('relatorios-estatisticas', 'HomeController@relatorios');
	Route::get('mensagens-notificacoes', 'HomeController@mensagensNotificacoes');

	Route::get('perfil-e-configuracoes', 'HomeController@perfil');

	Route::get('importacao', 'ImportacaoController@index');
	Route::post('importacao/upload', 'ImportacaoController@upload');

	Route::resource('usuarios', 'UsuariosController');

	Route::resource('representantes', 'RepresentantesController');
	Route::get('/representantes/buscar/{termoBuscado?}', 'RepresentantesController@index');

	Route::resource('cidades', 'CidadesController');
	Route::get('/cidades/buscar/{termoBuscado?}', 'CidadesController@index');
	Route::resource('cidades-representante', 'CidadesRepresentanteController');


	Route::get('/conteudo-notificacao/{id}', 'HomeController@getConteudoNotificacao');

	Route::get('/detalhes-pin/{id}', 'HomeController@detalhesPIN');




	// Rotas de configuração
	Route::match(['get', 'post'], '/filtra-mapa/{filtro?}', function($filtro = "ambos"){

		Session::put('filtro-home', $filtro);

		return Redirect::to('home');

	});




	// Rotas do usuario master
	Route::group(array('before' => 'auth.master'), function(){

	});

	// Rotas do usuario admin
	Route::group(array('before' => 'auth.admin'), function(){
		Route::resource('usuarios', 'UsuariosController');
	});

	// Rotas do usuario representante
	Route::group(array('before' => 'auth.representante'), function(){
	});
});
