<!doctype html>
<html lang="pt-BR">

   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <meta name="description" content="Sistema de Estatísticas e Relatórios Lubrilages">
      <meta name="author" content="Ueek Agência Digital">

      <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

      <title>Sistema - LUBRILAGES E NOVOLUBRI COM. LUBRIFICANTE</title>

      <!-- STYLES -->
      {{ HTML::style('css/bootstrap.min.css', array('type' => 'text/css')) }}
      {{ HTML::style('css/bootstrap-datepicker3.min.css', array('type' => 'text/css')) }}
      {{ HTML::style('css/bootbox.css', array('type' => 'text/css')) }}
      {{ HTML::style('css/font-awesome.min.css', array('type' => 'text/css')) }}
      {{ HTML::style('css/rdash.css', array('type' => 'text/css')) }}

      {{ HTML::style('plugins/fancybox/jquery.fancybox.css', array('type' => 'text/css')) }}

      {{ HTML::style('css/uploadifive.css', array('type' => 'text/css')) }}

      {{ HTML::style('css/main.css', array('type' => 'text/css')) }}

      @yield('css')

      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
         <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
         <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>

   <body>

      <nav id="barra-topo" class="navbar navbar-fixed-top">
         <div class="row header">
            <div class="col-xs-12">
               <div class="meta pull-left">
                  <img id="logotipo-sistema" src="/img/logo.png" alt="Sistema - Lubrilages">
               </div>
               <div class="user pull-right">
                  <div class="item dropdown">

                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="nome-usuario">Olá, {{ Auth::user()->nome }}</span>
                        <img src="/img/avatar.jpg">
                     </a>
                     <ul class="dropdown-menu dropdown-menu-right">
                        <li class="dropdown-header">
                           {{ Auth::user()->nome }} {{ Auth::user()->sobrenome }}
                        </li>
                        <li class="divider"></li>
                        <li class="link">
                           <a href="{{ URL::to('perfil-e-configuracoes') }}">Perfil e Configurações</a>
                        </li>
                        <li class="divider"></li>
                        <li class="link">
                           <a href="{{ URL::to('sair') }}">Sair</a>
                        </li>
                     </ul>
                  </div>

                  <?php
                     $quantidadeNotificacoes = Auth::user()->notifications()->unread()->count();

                     $topoNotificacoesNaoLidas = Auth::user()->notifications()->unread()->orderBy('created_at', 'desc')->take(5)->get();
                  ?>
                  <div class="item dropdown">
                     @if ( $quantidadeNotificacoes > 0 )
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                           <i class="fa fa-bell-o"></i>  <span id="qtd-notificacoes-atual">{{$quantidadeNotificacoes}}</span>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                           <li class="dropdown-header">
                              Notificações
                           </li>
                           <li class="divider"></li>
                           @foreach($topoNotificacoesNaoLidas as $topoNotificacao)
                              <li>
                                 <a href="#" data-value="{{ $topoNotificacao->id }}" class="alert-link">
                                    {{ $topoNotificacao->assunto }}
                                 </a>
                              </li>
                           @endforeach
                        </ul>
                     @else
                        <i class="fa fa-bell-o"></i>
                     @endif
                  </div>
               </div>
            </div>
         </div>
      </nav>

      <div id="page-wrapper" class="open">

         <!-- Sidebar -->
         <div id="sidebar-wrapper">
            <ul class="sidebar">
               <li id="toggle-sidebar" class="sidebar-main">
                  <a href="#">Menu<span class="menu-icon glyphicon glyphicon-transfer"></span></a>
               </li>
               <li class="sidebar-title">
                  <span>DASHBOARD</span>
               </li>
               <li class="sidebar-list">
                  <a href="{{ URL::to('/') }}">Home <span class="menu-icon fa fa-tachometer"></span></a>
               </li>

               @if ( Auth::user()->tipo != "representante" )
                  <li class="sidebar-list">
                     <a href="{{ URL::to('representantes') }}">Representantes <span class="menu-icon fa fa-users"></span></a>
                  </li>
               @endif

               @if ( Auth::user()->tipo != "representante" )
                  <li class="sidebar-list">
                     <a href="{{ URL::to('cidades') }}">Cidades <span class="menu-icon fa fa-map-marker"></span></a>
                  </li>
               @endif

               <li class="sidebar-list">
                  <a href="{{ URL::to('vendas') }}">Vendas <span class="menu-icon fa fa-usd"></span></a>
               </li>
               <li class="sidebar-list">
                  <a href="{{ URL::to('relatorios-estatisticas') }}">Relatórios e Estatísticas <span class="menu-icon fa fa-line-chart"></span></a>
               </li>
               <li class="sidebar-list">
                  <a href="{{ URL::to('mensagens-notificacoes') }}">Mensagens e Notificações <span class="menu-icon fa fa-comments-o"></span></a>
               </li>

               @if ( Auth::user()->tipo != "representante" )
                  <li class="sidebar-list">
                     <a href="{{ URL::to('usuarios') }}">Usuários <span class="menu-icon fa fa-user"></span></a>
                  </li>
               @endif

               @if ( Auth::user()->tipo != "representante" )
                  <li class="sidebar-list">
                     <a href="{{ URL::to('importacao') }}">Importação de Dados <span class="menu-icon fa fa-download"></span></a>
                  </li>
               @endif

            </ul>
            <div class="sidebar-footer">
               <div class="col-xs-8">
                  <a href="http://ueek.com.br" target="_blank">Ueek Agência Digital</a>
               </div>
               <div class="col-xs-4">
                  <a href="mailto:suporte@ueek.com.br">Suporte</a>
               </div>
            </div>
         </div>
         <!-- End Sidebar -->

         <div id="content-wrapper">
            <main class="page-content">

               <!-- Main Content -->
               @yield('conteudo')

            </main><!-- End Page Content -->
         </div><!-- End Content Wrapper -->
      </div><!-- End Page Wrapper -->



      <!-- SCRIPTS -->
      {{ HTML::script('js/jquery.min.js', array('type' => 'text/javascript')) }}
      {{ HTML::script('js/bootstrap.min.js', array('type' => 'text/javascript')) }}
      {{ HTML::script('js/bootbox.min.js', array('type' => 'text/javascript')) }}
      {{ HTML::script('js/jquery.cookie.js', array('type' => 'text/javascript')) }}
      {{ HTML::script('js/notify.min.js', array('type' => 'text/javascript')) }}

      {{ HTML::script('plugins/fancybox/jquery.fancybox.pack.js', array('type' => 'text/javascript')) }}

      {{ HTML::script('js/bootstrap-datepicker.min.js', array('type' => 'text/javascript')) }}
      {{ HTML::script('js/bootstrap-datepicker.pt-BR.min.js', array('type' => 'text/javascript')) }}

      {{ HTML::script('js/jquery.uploadifive.min.js', array('type' => 'text/javascript')) }}

      {{ HTML::script('js/app.js', array('type' => 'text/javascript')) }}
      {{ HTML::script('js/main.js', array('type' => 'text/javascript')) }}

      <script type="text/javascript">
         $(document).ready(function(){
            function verificaNotificacoes(){
               var qtdNotificacoesAtual = +$('#qtd-notificacoes-atual').html();
               var qtdNotificacoesSistema = {{ Auth::user()->notifications()->unread()->count() }};

               if(qtdNotificacoesSistema > qtdNotificacoesAtual){
                  $('#qtd-notificacoes-atual').html(qtdNotificacoesSistema);
                  //$('#qtd-notificacoes-atual').notify("Hello World", "success");
               }
            }
            setInterval(verificaNotificacoes, 5000);
         });
      </script>

      @yield('javascripts')


      <!-- Formulários dinâmicos -->
   	<script type="text/javascript">
   	$(document).ready(function(){

   		// Formulários de busca
   		$('form.buscar-ajax').on('submit', function(e){
   			e.preventDefault();

   			var urlBusca = $(this).attr('action');
   			var termoBuscado = $('input[name="buscar"]', $(this)).val();
   			if(termoBuscado == ""){
   				location.href=""+urlBusca+"";
   			}else{
   				location.href=""+urlBusca+"/buscar/"+termoBuscado+"";
   			}
   		});

   	});
   	</script>

   </body>

</html>
