<?php

class Notificacao extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'notificacoes';

	protected $fillable = ['id_usuario', 'tipo', 'assunto', 'texto', 'id_objeto', 'tipo_objeto', 'enviada_em'];

	private $relatedObject = null;

	/**
	 * Get the dates notifications.
	 *
	 * @return array
	 */
	public function getDates()
	{
		return ['created_at', 'updated_at', 'enviada_em'];
	}

	/**
	 * Get the user.
	 *
	 * @return object
	 */
	public function user()
	{
		return $this->belongsTo('Usuario', 'usuario_id');
	}

	/**
	 * Set sender notification.
	 *
	 * @return object
	 */
	public function sender()
	{
		return $this->belongsTo('Usuario', 'remetente_id');
	}

	/**
	 * Get unread notifications.
	 *
	 * @param  string  $query
	 * @return object
	 */
	public function scopeUnread($query)
	{
		return $query->where('lida', '=', 0);
	}

	/**
	 * Set subject notification.
	 *
	 * @param  string  $subject
	 * @return string
	 */
	public function withSubject($subject)
	{
		$this->assunto = $subject;

		return $this;
	}

	/**
	 * Set body notification.
	 *
	 * @param  string  $body
	 * @return string
	 */
	public function withBody($body)
	{
		$this->texto = $body;

		return $this;
	}

	/**
	 * Set type notification.
	 *
	 * @param  string  $type
	 * @return string
	 */
	public function withType($type)
	{
		$this->tipo = $type;

		return $this;
	}

	/**
	 * Set assossiated object notification.
	 *
	 * @param  string  $object
	 * @return string
	 */
	public function regarding($object)
	{
		if(is_object($object)){
			$this->id_objeto   = $object->id;
			$this->tipo_objeto = get_class($object);
		}

		return $this;
	}

	/**
	 * Associate "from" to notification.
	 *
	 * @return object
	 */
	public function from($user)
	{
		$this->sender()->associate($user);

		return $this;
	}

	/**
	 * Associate "to" to notification.
	 *
	 * @return object
	 */
	public function to($user)
	{
		$this->user()->associate($user);

		return $this;
	}

	/**
	 * Set send date notification.
	 *
	 * @return string
	 */
	public function deliver()
	{
		$this->enviada_em = new Carbon;
		$this->save();

		return $this;
	}

	public function hasValidObject()
	{
		try{
			$object = call_user_func_array($this->tipo_objeto . '::findOrFail', [$this->objeto_id]);
		}catch(Exception $e){
			return false;
		}

		$this->relatedObject = $object;

		return true;
	}

	public function getObject()
	{
		if(!$this->relatedObject){
			$hasObject = $this->hasValidObject();

			if(!$hasObject){
				throw new Exception(sprintf("Objeto inválido (%s com ID %s) assossiado com esta notificação.", $this->tipo_objeto, $this->objeto_id));
			}
		}

		return $this->relatedObject;
	}

}
