<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarNotificacao extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notificacoes', function(Blueprint $table)
		{
			$table->increments('id');
         $table->integer('usuario_id')->unsigned();

			$table->string('tipo', 128)->nullable();
         $table->string('assunto', 128)->nullable();
         $table->text('texto')->nullable();

         $table->integer('objeto_id')->unsigned();
         $table->string('tipo_objeto', 128);

         $table->boolean('lida')->default(0);
         $table->dateTime('enviada_em')->nullable();

         $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notificacoes');
	}

}
