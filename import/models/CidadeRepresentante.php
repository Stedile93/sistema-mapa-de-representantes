<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class CidadeRepresentante extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cidades_representante';

}
