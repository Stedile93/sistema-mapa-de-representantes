@extends('layouts.master')

@section('css')
@stop

@section('javascripts')
<script type="text/javascript">
$(document).ready(function(){

   // Editar dados de Perfil
   $('.formularios-perfil').on('submit', function(e){

      e.preventDefault();
      var form = $(this);

      var method = form.attr('method');
      var action = form.attr('action');

      var icone = $('button i', form);
      var botao = icone.parent();

      if($('input[name="tipo-form"]', form).val() == "perfil"){
         var email = $('input[name="email"]', form).val();
         if(!checkMail(email)){
            $('input[name="email"]', form).focus();
         }
      }


      // Show loading
      icone.attr('class', 'fa fa-spinner fa-spin');
      botao.attr('disabled', true);

      var valores = form.serialize();
      $.ajax({
         type     : method,
         url      : action,
         data     : valores,
         dataType : 'json',
         success  : function(data){
            // Hide loading
            botao.removeAttr('disabled');
            icone.attr('class', 'fa fa-plus fa-lg');

            if(data.status == 1){
               bootbox.alert({
                  closeButton: false,
                  message : data.msg
               });
            }else{
               bootbox.alert({
                  closeButton: false,
                  message : data.msg
               });
            }
         },
         error   : function(jq,status,message){
            bootbox.alert({
               closeButton: false,
               message : "Ocorreu um erro mais complexo que o normal, contate o desenvolvedor informando a seguinte mensagem: "+ status +" - "+ message,
               callback: function(){
                  // Hide loading
                  botao.removeAttr('disabled');
                  icone.attr('class', 'fa fa-plus fa-lg');
               }
            });
         }
      });

   });
});
</script>
@stop

@section('conteudo')

<!-- Header Bar -->
<div class="row header">
   <div class="col-xs-12">
      <div class="meta pull-left">
         <div class="page">
            Perfil e Configurações
         </div>
         <div class="breadcrumb-links">
            Home / Perfil e Configurações
         </div>
      </div>
   </div>
</div>
<!-- End Header Bar -->

<div class="row paddings-conteudo">

   <div class="col-md-6">
      <fieldset class="fieldset-form">
         <legend class="legend-form">Perfil</legend>

         {{ Form::model($usuario, array('route' => array('usuarios.update', $usuario->id), 'class' => 'formularios-perfil', 'method' => 'PUT')) }}

            {{ Form::hidden('tipo-form', 'perfil') }}

            <div class="form-group">
                {{ Form::label('nome', 'Nome') }}
                {{ Form::text('nome', null, array('class' => 'form-control', 'required')) }}
            </div>
            <div class="form-group">
                {{ Form::label('sobrenome', 'Sobrenome') }}
                {{ Form::text('sobrenome', null, array('class' => 'form-control', 'required')) }}
            </div>

            <div class="form-group">
               {{ Form::label('email', 'Email') }}
               {{ Form::email('email', null, array('class' => 'form-control', 'required')) }}
            </div>

            <div class="row">

               <div class="form-group col-md-6 no-padding-left">
                  {{ Form::label('data-nascimento', 'Data de Nascimento') }}

                  <?php
                  $dataNascimento = ($usuario->data_nascimento != "0000-00-00") ? Carbon::parse($usuario->data_nascimento)->format('d/m/Y') : null ;
                  ?>

                  <div class="input-group" id="datepicker">
                     {{ Form::text('data-nascimento', $dataNascimento, array('class' => 'datepicker form-control', 'placeholder' => '__/__/__', 'maxlength' => '8', 'onkeydown' => 'Mascara(this,Data);', 'onkeypress' => 'Mascara(this,Data);', 'onkeyup' => 'Mascara(this,Data);', 'required')) }}
                     <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                  </div>
               </div>

               <div class="form-group col-md-6 no-padding-right">
                  {{ Form::label('cpf', 'CPF') }}
                  {{ Form::text('cpf', null, array('class' => 'form-control', 'placeholder' => '___.___.___-__', 'maxlength' => '14', 'onkeydown' => 'Mascara(this,Cpf);', 'onkeypress' => 'Mascara(this,Cpf);', 'onkeyup' => 'Mascara(this,Cpf);',  'required')) }}
               </div>

            </div>

            {{ Form::button('<i class="fa fa-plus"></i> Salvar', array('type' => 'submit', 'class' => 'btn btn-success pull-right')) }}

         {{ Form::close() }}
      </fieldset>
   </div>

   <div class="col-md-6">
      <fieldset class="fieldset-form">
         <legend class="legend-form">Imagen</legend>

         <p>
            Em breve, imagem de perfil!
         </p>

         <!--
         {{ Form::model($usuario, array('route' => array('usuarios.update', $usuario->id), 'id' => 'form-perfil', 'method' => 'PUT')) }}

            <div class="form-group">
                {{ Form::label('nome', 'Nome') }}
                {{ Form::text('nome', null, array('class' => 'form-control', 'required')) }}
            </div>


            {{ Form::button('<i class="fa fa-plus"></i> Salvar', array('type' => 'submit', 'class' => 'btn btn-success pull-right')) }}

         {{ Form::close() }}
         -->
      </fieldset>
   </div>

</div>


<div class="row">
   <div class="col-md-6">

      <fieldset class="fieldset-form row">
         <legend class="legend-form">Alterar Senha</legend>

         {{ Form::model($usuario, array('route' => array('usuarios.update', $usuario->id), 'class' => 'formularios-perfil', 'method' => 'PUT')) }}

            {{ Form::hidden('tipo-form', 'senha') }}

            <div class="form-group col-md-6 no-padding-left">
               {{ Form::label('nova-senha', 'Nova Senha') }}
               {{ Form::password('nova-senha', array('class' => 'form-control', 'maxlength' => '20', 'minlength' => '6', 'required')) }}
            </div>

            <div class="form-group col-md-6 no-padding-right">
               {{ Form::label('re-nova-senha', 'Repetir Nova Senha') }}
               {{ Form::password('re-nova-senha', array('class' => 'form-control', 'required')) }}
            </div>


            {{ Form::button('<i class="fa fa-plus"></i> Salvar', array('type' => 'submit', 'class' => 'btn btn-success pull-right')) }}

         {{ Form::close() }}
      </fieldset>

   </div>

   <div class="col-md-6">

      <fieldset class="fieldset-form row">
         <legend class="legend-form">Endereço</legend>

         {{ Form::model($usuario, array('route' => array('usuarios.update', $usuario->id), 'class' => 'formularios-perfil', 'method' => 'PUT')) }}

            {{ Form::hidden('tipo-form', 'endereco') }}

            <div class="form-group col-md-6 no-padding-left">
               {{ Form::label('uf', 'Estado') }}
               {{ Form::select('uf', array(
                     '0' => 'UF',
                     'AC' => 'Acre',
                     'AL' => 'Alagoas',
                     'AM' => 'Amazonas',
                     'AP' => 'Amapá',
                     'BA' => 'Bahia',
                     'CE' => 'Ceará',
                     'DF' => 'Distrito Federal',
                     'ES' => 'Espírito Santo',
                     'GO' => 'Goiás',
                     'MA' => 'Maranhão',
                     'MT' => 'Mato Grosso',
                     'MS' => 'Mato Grosso do Sul',
                     'MG' => 'Minas Gerais',
                     'PA' => 'Pará',
                     'PB' => 'Paraíba',
                     'PR' => 'Paraná',
                     'PE' => 'Pernambuco',
                     'PI' => 'Piauí',
                     'RJ' => 'Rio de Janeiro',
                     'RN' => 'Rio Grande do Norte',
                     'RO' => 'Rondônia',
                     'RS' => 'Rio Grande do Sul',
                     'RR' => 'Roraima',
                     'SC' => 'Santa Catarina',
                     'SE' => 'Sergipe',
                     'SP' => 'São Paulo',
                     'TO' => 'Tocantin'
                  ), null, array('class' => 'form-control')) }}
            </div>
            <div class="form-group col-md-6 no-padding-right">
               {{ Form::label('cidade', 'Cidade') }}
               {{ Form::text('cidade', null, array('class' => 'form-control', 'required')) }}
            </div>

            {{ Form::button('<i class="fa fa-plus"></i> Salvar', array('type' => 'submit', 'class' => 'btn btn-success pull-right')) }}

         {{ Form::close() }}
      </fieldset>
   </div>
</div>

<!--
<div class="row">

   <div class="col-xs-12">

      <fieldset class="fieldset-form">
         <legend class="legend-form">Configurações</legend>

         {{ Form::model($usuario, array('route' => array('usuarios.update', $usuario->id), 'id' => 'form-config', 'method' => 'PUT')) }}

            {{ Form::hidden('tipo-form', 'config') }}

            <div class="form-group">
               {{ Form::checkbox('notificacao-email', '1') }} Desejo receber minhas notificações por e-mail também.
            </div>

            {{ Form::button('<i class="fa fa-plus"></i> Salvar', array('type' => 'submit', 'class' => 'btn btn-success pull-right')) }}

         {{ Form::close() }}
      </fieldset>


   </div>
</div>
-->

@stop
