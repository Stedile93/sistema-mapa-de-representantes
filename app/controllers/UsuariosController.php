<?php

class UsuariosController extends BaseController {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Auth::user()->tipo == "representante")
			return Redirect::to('/');

		$usuarios = Usuario::where('tipo', '<>', 'representante')->orderBy('nome', 'asc')->paginate(15);

		return View::make('usuarios.index')->with('usuarios', $usuarios);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if(Auth::user()->tipo == "representante")
			return Redirect::to('/');

		return View::make('usuarios.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if(empty(Input::get('nome')) or empty(Input::get('sobrenome')) or empty(Input::get('email')) or empty(Input::get('login')) or empty(Input::get('data-nascimento')) or empty(Input::get('cpf')) or empty(Input::get('cidade'))){
			return Response::json(['status' => 0, 'msg' => 'Todos os dados precisam ser preenchidos!']);
		}

		$usuario = new Usuario();
      $usuario->nome      = Input::get('nome');
      $usuario->sobrenome = Input::get('sobrenome');
      $usuario->email     = Input::get('email');
      $usuario->login     = Str::lower(Input::get('login'));
      $usuario->senha     = Hash::make(Input::get('senha'));

		if( !empty(Input::get('data-nascimento')) ){
			$usuario->data_nascimento = ConversaoData::textToSql(Input::get('data-nascimento'));
		}

      $usuario->cpf       = Input::get('cpf');

		if(Input::get('uf') == '0'){
			return Response::json(['status' => 0, 'msg' => 'Selecione um estado!']);
		}

		$usuario->uf        = Input::get('uf');
		$usuario->cidade    = Input::get('cidade');

		if(Input::get('tipo') == '0'){
			return Response::json(['status' => 0, 'msg' => 'Selecione um tipo de usuário!']);
		}

		$usuario->tipo      = Input::get('tipo');
      $usuario->cor       = Usuario::randomColor();

		if($usuario->save()){
			return Response::json(['status' => 1, 'msg' => 'Usuário cadastrado com sucesso!']);
		}else{
			return Response::json(['status' => 0, 'msg' => 'Ocorreu algum erro ao cadastrar este usuário!']);
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if(Auth::user()->tipo == "representante")
			return Redirect::to('/');

		$usuario = Usuario::find($id);
		return View::make('usuarios.show')->with('usuario', $usuario);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if(Auth::user()->tipo == "representante")
			return Redirect::to('/');

		$usuario = Usuario::find($id);
		return View::make('usuarios.edit')->with('usuario', $usuario);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$usuario = Usuario::find($id);

		if(Input::get('tipo-form') == "perfil"){

			if(empty(Input::get('nome')) or empty(Input::get('sobrenome')) or empty(Input::get('email')) or empty(Input::get('senha')) or empty(Input::get('data-nascimento')) or empty(Input::get('cpf'))){
				return Response::json(['status' => 0, 'msg' => 'Todos os dados precisam ser preenchidos!']);
			}

			$usuario->nome      = Input::get('nome');
	      $usuario->sobrenome = Input::get('sobrenome');
	      $usuario->email     = Input::get('email');
			if( !empty(Input::get('data-nascimento')) ){
				$usuario->data_nascimento = ConversaoData::textToSql(Input::get('data-nascimento'));
			}
	      $usuario->cpf       = Input::get('cpf');

		}elseif(Input::get('tipo-form') == "senha"){

			if(Input::get('nova-senha') != Input::get('re-nova-senha')){
				return Response::json(['status' => 0, 'msg' => 'Senha de confirmação inválida.']);
			}

	      $usuario->senha = Hash::make(Input::get('nova-senha'));

		}elseif(Input::get('tipo-form') == "endereco"){

			if(Input::get('uf') == '0'){
				return Response::json(['status' => 0, 'msg' => 'Selecione um estado!']);
			}

			$usuario->uf     = Input::get('uf');
	      $usuario->cidade = Input::get('cidade');

		}else{

			if(empty(Input::get('nome')) or empty(Input::get('sobrenome')) or empty(Input::get('email')) or empty(Input::get('data-nascimento')) or empty(Input::get('cpf')) or empty(Input::get('cidade'))){
				return Response::json(['status' => 0, 'msg' => 'Todos os dados precisam ser preenchidos!']);
			}

			$usuario->nome      = Input::get('nome');
	      $usuario->sobrenome = Input::get('sobrenome');
	      $usuario->email     = Input::get('email');

			if( !empty(Input::get('data-nascimento')) ){
				$usuario->data_nascimento = ConversaoData::textToSql(Input::get('data-nascimento'));
			}
	      $usuario->cpf = Input::get('cpf');

			if(Input::get('uf') == '0'){
				return Response::json(['status' => 0, 'msg' => 'Selecione um estado!']);
			}

			$usuario->uf     = Input::get('uf');
	      $usuario->cidade = Input::get('cidade');

			if(Input::get('tipo') == '0'){
				return Response::json(['status' => 0, 'msg' => 'Selecione um tipo de usuário!']);
			}

			$usuario->tipo     = Input::get('tipo');

		}

		$mensagensSucesso = "";
		$mensagensErro    = "";
		switch (Input::get('tipo-form')) {
			case 'perfil':
				$mensagensSucesso = "Dados de perfil alterados com sucesso!";
				$mensagensErro    = "Ocorreu algum erro ao alterar dados do perfil!";
				break;

			case 'senha':
				$mensagensSucesso = "Nova senha salva com sucesso!";
				$mensagensErro    = "Ocorreu algum erro ao alterar senha!";
				break;

			case 'endereco':
				$mensagensSucesso = "Endereço alterado com sucesso!";
				$mensagensErro    = "Ocorreu algum erro ao alterar endereço!";
				break;

			default:
				$mensagensSucesso = "Dados de usuário alterado com sucesso!";
				$mensagensErro    = "Ocorreu algum erro ao alterar este usuário!";
				break;
		}

		if($usuario->save()){
			return Response::json(['status' => 1, 'msg' => $mensagensSucesso]);
		}else{
			return Response::json(['status' => 0, 'msg' => $mensagensErro]);
		}

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$usuario = Usuario::find($id);

		if($usuario->tipo == "representante"){
			return Response::json(['status' => 0, 'msg' => 'Ocorreu algum erro ao remover este usuário! Esta ação não é permitida.']);
		}

		if($usuario->delete()){
			return Response::json(['status' => 1, 'msg' => 'Usuário removido con sucesso!']);
		}else{
			return Response::json(['status' => 0, 'msg' => 'Ocorreu algum erro ao remover este usuário!']);
		}
	}

}
