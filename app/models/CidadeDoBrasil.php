<?php

class CidadeDoBrasil extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cidades_do_brasil';

	public $timestamps = false;

}
