<?php

class CidadesRepresentanteController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if(empty(Input::get('id-representante')) or Input::get('id-representante') == 0 or empty(Input::get('cidade-representante')) or Input::get('cidade-representante') == 0){
			return Response::json(['status' => 0, 'msg' => 'Selecione uma cidade para cadastrar!']);
		}

		$cidadeRepresentante = new CidadeRepresentante();
      $cidadeRepresentante->id_usuario = Input::get('id-representante');
      $cidadeRepresentante->id_cidade   = Input::get('cidade-representante');

		if($cidadeRepresentante->save()){
			return Response::json(['status' => 1, 'msg' => 'Cidade cadastrada com sucesso para este representante!']);
		}else{
			return Response::json(['status' => 0, 'msg' => 'Ocorreu algum erro ao cadastrar esta cidade para este representante!']);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$cidade = CidadeRepresentante::find($id);

		if($cidade->delete()){
			return Response::json(['status' => 1, 'msg' => 'Cidade removida com sucesso deste representante!']);
		}else{
			return Response::json(['status' => 0, 'msg' => 'Ocorreu algum erro ao remover esta cidade deste representante!']);
		}
	}


}
