<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemetenteIdEmNotificacoes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notificacoes', function(Blueprint $table)
		{
         $table->integer('remetente_id')->nullable()->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notificacoes', function(Blueprint $table)
		{
			$table->dropColumn('remetente_id');
		});
	}

}
