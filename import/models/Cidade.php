<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Cidade extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cidades';


	public static function pegaLocationCidade($cidade){
		$cidade = urlencode(trim($cidade));

		$url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address={$cidade}&region=BR";

		$resposta_json = file_get_contents($url);
		$resposta = json_decode($resposta_json, true);

		if($resposta['status'] == 'OK' && count($resposta['results']) > 0){

			// get the important data
			$latitude  = $resposta['results'][0]['geometry']['location']['lat'];
			$longitude = $resposta['results'][0]['geometry']['location']['lng'];

			$endereco = $resposta['results'][0]['formatted_address'];

			list($nomeCidade, $resto) = explode(" - ", $endereco);
			list($ufCidade, $lixo) = explode(",", $resto);

			// verify if data is complete
			if($latitude && $longitude && $nomeCidade){

				$returno = array(
					"nome-cidade" => $nomeCidade,
					"uf-cidade" => $ufCidade,
					"latitude" => $latitude,
					"longitude" => $longitude
				);

            return $returno;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public static function verificaCidadePorNome($nome)
	{

		$dadosCidade = $this->pegaLocationCidade(trim($nome));

		if(Cidade::where('nome' , '=', $dadosCidade['nome-cidade'])->count() == 1){
			return true;
		}else{
			return false;
		}

	}

}
