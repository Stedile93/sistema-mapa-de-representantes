@extends('layouts.master')


@section('javascripts')
@stop

@section('conteudo')

<!-- Header Bar -->
<div class="row header">
   <div class="col-xs-12">
      <div class="meta pull-left">
         <div class="page">
            Vendas
         </div>
         <div class="breadcrumb-links">
            Home / Vendas
         </div>
      </div>
   </div>
</div>
<!-- End Header Bar -->

<div class="row paddings-conteudo">
   <div class="col-xs-12">

      <table class="table table-striped">
         <thead>
            <tr>
               <td>Representante</td>
               <td>Cidade</td>
               <td>Valor Vendido</td>
            </tr>
         </thead>
         <tbody>
            @foreach($vendas as $key => $value)
            <tr>
               <td>{{ Usuario::find($value->id_usuario)->nome }}</td>
               <td>{{ Cidade::find($value->id_cidade)->nome }}</td>
               <td>R$ {{ number_format($value->valor_vendido, 2, ',', '.') }}</td>
            </tr>
            @endforeach
         </tbody>
      </table>

      {{ $vendas->links() }}

   </div>
</div>

@stop
