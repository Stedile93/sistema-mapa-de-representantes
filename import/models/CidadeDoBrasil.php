<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class CidadeDoBrasil extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cidades_do_brasil';

}
