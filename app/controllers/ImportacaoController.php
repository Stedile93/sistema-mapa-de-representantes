<?php

set_time_limit(0);

class ImportacaoController extends BaseController {

	protected $qtdLinhas = 0;
	protected $qtdLinhasSucesso = 0;
	protected $qtdLinhasDuplicadas = 0;
	protected $qtdLinhasComErro = 0;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Auth::user()->tipo == "representante")
			return Redirect::to('/');

		$importacoes = Importacao::orderBy('created_at', 'desc')->paginate(10);

		return View::make('dashboard.importacao')->with('importacoes', $importacoes);
	}


	public function upload()
	{

		$arquivo = Input::file('Filedata');

		$extencao = $arquivo->getClientOriginalExtension();

		if($extencao == "txt"){


			$destino = 'uploads/';
			$nomeArquivoOriginal = substr($arquivo->getClientOriginalName(), 0, -3);
			$nomeArquivo = Str::slug(Carbon::now().' '.$nomeArquivoOriginal).".".$extencao;

			if(Input::file('Filedata')->move($destino, $nomeArquivo)){
				$idImportacao = $this->registraImportacao( DB::raw('NOW()'), $destino.$nomeArquivo, Str::lower( $arquivo->getClientOriginalName() ) );
				if( !$idImportacao ){

					return Response::json(['success' => false, 'msg' => 'Falha ao registrar arquivo!']);

				}else{

					if( $this->extrairDadosArquivo($idImportacao, asset($destino.$nomeArquivo)) ){

						$importacao = Importacao::find($idImportacao);
						$importacao->linhas_analisadas = $this->qtdLinhas;
						$importacao->linhas_importadas = $this->qtdLinhasSucesso;
						$importacao->linhas_duplicadas = $this->qtdLinhasDuplicadas;
						$importacao->linhas_com_erro   = $this->qtdLinhasComErro;
						$importacao->status            = 2;
						$importacao->save();

						$caminhoAbsoluto = str_replace("/public", "", $_SERVER['DOCUMENT_ROOT']);
						exec("php ".$caminhoAbsoluto."/import/importacao.php ".$idImportacao.""); // /opt/php55/bin/php /home/ueekc672/public_html/lubrilages/import/importacao.php $idImportacao > /dev/null &

						return Response::json(['success' => true, 'msg' => 'Arquivo importado com sucesso!']);
					}else{
						return Response::json(['success' => false, 'msg' => 'Falha ao importar arquivo!']);
					}

				}
			}else{
				return Response::json(['success' => false, 'msg' => 'Falha ao carregar arquivo!']);
			}
		}else{
			return Response::json(['success' => false, 'msg' => 'Tipo de arquio inválido! É necessário que o arquivo seja de texto e no padrão de layout da lubrilages.']);
		}
	}

	public function registraImportacao($dataImportacao, $caminhoArquivo, $nomeArquivo)
	{
		$importacao = new Importacao();
		$importacao->id_usuario      = Auth::user()->id;
		$importacao->data_importacao = $dataImportacao;
		$importacao->arquivo         = $caminhoArquivo;
		$importacao->nome            = $nomeArquivo;
		if($importacao->save()){
			return $importacao->id;
		}else{
			return false;
		}
	}

	public function extrairDadosArquivo($idImportacao, $caminhoArquivo)
	{
		$arquivo = fopen($caminhoArquivo, 'r');

		Log::info("Inicio da importação #{$idImportacao}");

		while(!feof($arquivo)) {
			$linha = fgets($arquivo, 4096);

			$codigo = substr($linha, 0, 6);

			if($codigo == "000001"){ // Dados do arquivo de importação

				$data = substr($linha, 7, 10);
				$hora = substr($linha, 17, 8);

				$importacao = Importacao::find($idImportacao);
				$importacao->data_importacao = $data." ".$hora;
				$importacao->save();

			}elseif($codigo == "000002"){ // Dados dos vendedores da importação

				$data          = trim(substr($linha, 7, 10));
				$representante = Str::title( trim(substr($linha, 17, 23)) );
				$cidade        = trim(substr($linha, 40, 30));
				$valor         = substr($linha, 71, 10);

				$valor = str_replace(".", "", $valor);
				$valor = (float) str_replace(",", ".", $valor);

				$this->registraDadosImportacao($idImportacao, $data, $representante, $cidade, $valor);
			}

			$this->qtdLinhas++;
		}

		fclose($arquivo);

		return true;
	}

	public function registraDadosImportacao($idImportacao, $data, $representante, $cidade, $valor)
	{
		if(DadosImportacao::where('data' , '=', $data)->where('vendedor' , '=', $representante)->where('cidade' , '=', $cidade)->where('valor_vendido' , '=', $valor)->count() == 0){
			$dadosImportacao = new DadosImportacao();
			$dadosImportacao->id_importacao = $idImportacao;
			$dadosImportacao->data          = $data;
			$dadosImportacao->vendedor      = $representante;
			$dadosImportacao->cidade        = $cidade;
			$dadosImportacao->valor_vendido = $valor;

			if($dadosImportacao->save()){
				$this->qtdLinhasSucesso++;
			}else{
				$this->qtdLinhasErro++;
			}
		}else{
			$this->qtdLinhasDuplicadas++;
		}
	}


}
