$(document).ready(function(){

   $.notify.defaults({ arrowShow: false });

   $(".fancybox").fancybox({
		maxWidth	   : 800,
		maxHeight	: 600,
		fitToView	: false,
		width		   : '70%',
		height		: '70%',
		autoSize	   : false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});


   $('.datepicker').datepicker({
      format: 'dd/mm/yyyy',
      container: '#datepicker',
      orientation: 'top',
      language: 'pt-BR'
   });


   $('a.alert-link').on('click', function(e){

      e.preventDefault();

      var link = $(this);
      var idNtf = link.attr('data-value');

      $.ajax({
         type: 'GET',
         url: '/conteudo-notificacao/'+idNtf+'',
         dataType: 'json',
         success: function(data){
            bootbox.alert({
               closeButton: false,
               message : '<h3>'+data.assunto+'</h3><br><p>'+data.texto+'</p>',
               callback: function(){
                  var alert = link.parent().attr('role');
                  if(alert == 'alert'){
                     link.parent().addClass('alert-success');
                     link.parent().removeClass('alert-warning');
                  }
               }
            });
         }
      });
   });

});




//Máscaras
function Mascara(o,f){
	v_obj=o
	v_fun=f
	setTimeout("execmascara()",1)
}

function execmascara(){
	v_obj.value=v_fun(v_obj.value)
}

function leech(v){
	v=v.replace(/o/gi,"0")
	v=v.replace(/i/gi,"1")
	v=v.replace(/z/gi,"2")
	v=v.replace(/e/gi,"3")
	v=v.replace(/a/gi,"4")
	v=v.replace(/s/gi,"5")
	v=v.replace(/t/gi,"7")
	return v
}

function Integer(v){
	return v.replace(/\D/g,"")
}

function Login(v){
	return v.replace(/\W/g,"")
}

function Telefone(v){
	v=v.replace(/\D/g,"")
	v=v.replace(/^(\d\d)(\d)/g,"($1) $2")
	v=v.replace(/(\d{4})(\d)/,"$1-$2")
	return v
}

function Cpf(v){
	v=v.replace(/\D/g,"")
	v=v.replace(/(\d{3})(\d)/,"$1.$2")
	v=v.replace(/(\d{3})(\d)/,"$1.$2")
	v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2")
	return v
}

function Cnpj(v){
	v=v.replace(/\D/g,"")
	v=v.replace(/^(\d{2})(\d)/,"$1.$2")
	v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3")
	v=v.replace(/\.(\d{3})(\d)/,".$1/$2")
	v=v.replace(/(\d{4})(\d)/,"$1-$2")
	return v
}

function Cep(v){
	v=v.replace(/\D/g,"")
	v=v.replace(/^(\d{5})(\d)/,"$1-$2")
	return v
}

function Data(v){
	v=v.replace(/\D/g,"")
	v=v.replace(/(\d{2})(\d)/,"$1/$2")
	v=v.replace(/(\d{2})(\d)/,"$1/$2")
	return v
}

function checkMail(mail){
	var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
	if(typeof(mail) == "string"){
		if(er.test(mail)){ return true; }
	}else if(typeof(mail) == "object"){
		if(er.test(mail.value)){
			return true;
		}
	}else{
		return false;
	}
}
