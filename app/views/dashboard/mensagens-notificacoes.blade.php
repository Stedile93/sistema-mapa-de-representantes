@extends('layouts.master')


@section('javascripts')
@stop

@section('conteudo')

<!-- Header Bar -->
<div class="row header">
   <div class="col-xs-12">
      <div class="meta pull-left">
         <div class="page">
            Mensagens e Notificações
         </div>
         <div class="breadcrumb-links">
            Home / Mensagens e Notificações
         </div>
      </div>
   </div>
</div>
<!-- End Header Bar -->

<div class="row paddings-conteudo">
   <div class="col-xs-12">

      @foreach($notificacoes as $notificacao)
         <div class="alert @if($notificacao->lida == 0) alert-warning @else alert-success @endif" role="alert">
            <a href="#" data-value="{{ $notificacao->id }}" class="alert-link">
               {{ date('d/m/Y H:i:s', strtotime($notificacao->created_at)) }}: {{ $notificacao->assunto }}
            </a>
         </div>
      @endforeach


      {{ $notificacoes->links() }}

   </div>
</div>

@stop
