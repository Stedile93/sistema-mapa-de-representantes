<?php
use Illuminate\Support\Str as Str;

function cadastrarRepresentante($nomeRepresentante)
{
	if(!empty($nomeRepresentante)){

		$nomeRepresentante = trim($nomeRepresentante);

		$loginUsuario = Str::lower(str_replace('-', '_', Str::slug($nomeRepresentante)));

		if(Usuario::where('login' , '=', $loginUsuario)->count() == 0){

			$usuario = new Usuario();
			$usuario->nome  = $nomeRepresentante;
			$usuario->login = $loginUsuario;
			$usuario->senha = ConfigSistema::find(1)->hash_senha_padrao;
			$usuario->cor   = Usuario::randomColor();
			$usuario->tipo  = "representante";

			if($usuario->save()){
				return $usuario->id;
			}else{
				return false;
			}

		}else{
			return Usuario::where('login' , '=', $loginUsuario)->first()->id;
		}

	}
}


function cadastrarCidade($nomeCidade)
{

	$dadosGoogle = Cidade::pegaLocationCidade($nomeCidade);

	if($dadosGoogle){

		if(Cidade::where('nome' , '=', $dadosGoogle['nome-cidade'])->count() == 0){

			$cidade = new Cidade();

			$cidade->nome      = $dadosGoogle['nome-cidade'];
			$cidade->uf        = $dadosGoogle['uf-cidade'];
			$cidade->latitude  = $dadosGoogle['latitude'];
			$cidade->longitude = $dadosGoogle['longitude'];
			$cidade->status    = 1;

			$habitantesEVeiculos = CidadeDoBrasil::where('uf', '=', $cidade->uf)->where('cidade', 'LIKE', '%'.$cidade->nome.'%')->first();

			if($habitantesEVeiculos->populacao > 0){
				$cidade->habitantes = $habitantesEVeiculos->populacao;
			}
			if($habitantesEVeiculos->frota_veiculos > 0){
				$cidade->veiculos = $habitantesEVeiculos->frota_veiculos;
			}

			if($cidade->save()){
				return $cidade->id;
			}else{
				return false;
			}

		}else{
			return Cidade::where('nome' , '=', $dadosGoogle['nome-cidade'])->first()->id;
		}
	}
}


function cadastrarValorVendido($representante, $cidade, $data, $valor)
{

	if($representante > 0 and $cidade > 0 and !empty($data)){

		$valorVendido = new ValorVendido();
		$valorVendido->id_usuario    = $representante;
		$valorVendido->id_cidade     = $cidade;
		$valorVendido->data          = $data;
		$valorVendido->valor_vendido = $valor;

		if($valorVendido->save()){
			return $valorVendido->id;
		}else{
			return false;
		}

	}
}

function cadastrarCidadeRepresentante($cidade, $representante)
{

	if(CidadeRepresentante::where('id_usuario' , '=', $representante)->where('id_cidade' , '=', $cidade)->count() == 0){
		$cidadeRep = new CidadeRepresentante();
		$cidadeRep->id_usuario = $representante;
		$cidadeRep->id_cidade  = $cidade;

		if($cidadeRep->save()){
			return $cidadeRep->id;
		}else{
			return false;
		}
	}
}
