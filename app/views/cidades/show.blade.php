@extends('layouts.master')


@section('javascripts')
@stop

@section('conteudo')

<!-- Header Bar -->
<div class="row header">
   <div class="col-xs-12">
      <div class="meta pull-left">
         <div class="page">
            {{ $cidade->nome }}
         </div>
         <div class="breadcrumb-links">
            Home / Cidades / {{ $cidade->nome }}
         </div>
      </div>
   </div>
</div>
<!-- End Header Bar -->


<div class="row paddings-conteudo">
   <div class="col-xs-12">

      <a href="{{ URL::to('cidades/create') }}">
         <button type="button" class="btn btn-success">Adicionar Cidade</button>
      </a>

      <div class="jumbotron text-center">
         <h2>{{ $cidade->nome }}</h2>
         <p>
            <strong>Sobrenome:</strong> {{ $cidade->sobrenome }}<br>
            <strong>Email:</strong> {{ $cidade->email }}
         </p>
      </div>
   </div>
</div>

@stop
