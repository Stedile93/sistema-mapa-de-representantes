<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableImportacoes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('importacoes', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('id_usuario')->unsigned();
	      $table->foreign('id_usuario')->references('id')->on('usuarios');

			$table->dateTime('data_importacao');

			$table->integer('linhas_analisadas');
			$table->integer('linhas_importadas');
			$table->integer('linhas_duplicadas');
			$table->integer('linhas_com_erro');

			$table->enum('status', array('0', '1', '2'))->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('importacoes');
	}

}
