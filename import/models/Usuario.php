<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Usuario extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'usuarios';

	public function newNotification()
	{
		$notification = new Notificacao;
		$notification->user()->associate($this);

		return $notification;
	}

	public static function randomColor()
	{
		$chars = '0123456789ABCDEF';
		$cor = '#';
		for($i = 0; $i < 6; $i++) {
			$char = rand(0,15);
			$cor .= $chars[$char];
		}
		return $cor;
	}

}
