@extends('layouts.master')


@section('javascripts')
<script type="text/javascript">
   $(document).ready(function(){

      $('a.botao-remover').on('click', function(e){

         e.preventDefault();

         var linha = $(this).parent().parent();
         var icone = $('i', $(this));

         var idUsuario = $(this).attr('data-id');

         bootbox.confirm({
				closeButton: false,
				message : "Deseja realmente remover permanentemente este usuário? Esta ação é irreversível.",
				callback : function(result) {
					if(result == true){

                  // Show loading
                  icone.attr('class', 'fa fa-spinner fa-spin');

						$.ajax({
							type     : 'POST',
							url      : 'usuarios/'+idUsuario,
							data     : {'_method' : 'DELETE'},
							dataType : 'json',
							success  : function(data){
                        // Hide loading
                        icone.attr('class', 'fa fa-remove fa-lg');

								if(data.status == 1){
                           linha.fadeOut();
								}else{
									bootbox.alert({
										closeButton: false,
										message : data.msg
									});
								}
							}
						});
					}
				}
			});

      });

   });
</script>
@stop

@section('conteudo')

<!-- Header Bar -->
<div class="row header">
   <div class="col-xs-8">
      <div class="meta pull-left">
         <div class="page">
            Usuários
         </div>
         <div class="breadcrumb-links">
            Home / Usuários
         </div>
      </div>
   </div>

   <div class="col-xs-4 btn-header">
      <a class="pull-right" href="{{ URL::to('usuarios/create') }}">
         Adicionar Usuário <i class="fa fa-plus fa-lg"></i>
      </a>
   </div>
</div>
<!-- End Header Bar -->


<div class="row paddings-conteudo">
   <div class="col-xs-12">

      <table class="table table-striped">
         <thead>
            <tr>
               <td class="coluna-acoes text-center">Ações</td>
               <td>Name</td>
               <td>Email</td>
               <td>Tipo</td>
               <td class="coluna-remover text-center"></td>
            </tr>
         </thead>
         <tbody>
            @foreach($usuarios as $key => $value)
            <tr>
               <td class="text-center">
                  <a class="hover-verde pull-left margin-left-15" href="{{ URL::to('usuarios/' . $value->id . '/edit') }}" title="Editar">
                     <i class="fa fa-edit fa-lg"></i> Ver/Editar
                  </a>
               </td>

               <td>{{ $value->nome }}</td>
               <td>{{ $value->email }}</td>
               <td>{{ $value->tipo }}</td>

               <td class="text-center">
                  <a class="botao-remover hover-vermelho" href="#remover" data-id="{{ $value->id }}" title="Remover">
                     Remover <i class="fa fa-remove fa-lg"></i>
                  </a>
               </td>
            </tr>
            @endforeach
         </tbody>
      </table>

      {{ $usuarios->links() }}
   </div>
</div>

@stop
