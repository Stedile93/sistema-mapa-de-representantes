@extends('layouts.master')


@section('javascripts')
<script type="text/javascript">
$(document).ready(function(){

   // Editar dados de Perfil
   $('form[name="form-cadastro"]').on('submit', function(e){

      e.preventDefault();
      var form = $(this);

      var method = form.attr('method');
      var action = form.attr('action');

      var icone = $('button i', form);
      var botao = icone.parent();




      // Show loading
      icone.attr('class', 'fa fa-spinner fa-spin');
      botao.attr('disabled', true);

      var valores = form.serialize();
      $.ajax({
         type     : method,
         url      : action,
         data     : valores,
         dataType : 'json',
         success  : function(data){
            // Hide loading
            botao.removeAttr('disabled');
            icone.attr('class', 'fa fa-plus fa-lg');

            if(data.status == 1){
               bootbox.alert({
                  closeButton: false,
                  message : data.msg,
                  callback : function(){
                     location.href="{{ URL::to('cidades') }}";
                  }
               });
            }else{
               bootbox.alert({
                  closeButton: false,
                  message : data.msg
               });
            }
         },
         error   : function(jq,status,message){
            bootbox.alert({
               closeButton: false,
               message : "Ocorreu um erro mais complexo que o normal, contate o desenvolvedor informando a seguinte mensagem: "+ status +" - "+ message,
               callback: function(){
                  // Hide loading
                  botao.removeAttr('disabled');
                  icone.attr('class', 'fa fa-plus fa-lg');
               }
            });
         }
      });

   });

});
</script>
@stop

@section('conteudo')

<!-- Header Bar -->
<div class="row header">
   <div class="col-xs-12">
      <div class="meta pull-left">
         <div class="page">
            Adicionar Cidade
         </div>
         <div class="breadcrumb-links">
            Home / Cidades / Adicionar Cidade
         </div>
      </div>
   </div>
</div>
<!-- End Header Bar -->


{{ Form::open(array('url' => 'cidades', 'name' => 'form-cadastro')) }}

   <div class="row paddings-conteudo">

      <div class="col-md-3">
         <div class="form-group">
            {{ Form::label('nome', 'Nome da Cidade') }}
            {{ Form::text('nome', Input::old('nome'), array('class' => 'form-control', 'required')) }}
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            {{ Form::label('uf', 'Estado') }}
            {{ Form::select('uf', array(
               '0' => 'UF',
               'AC' => 'Acre',
               'AL' => 'Alagoas',
               'AM' => 'Amazonas',
               'AP' => 'Amapá',
               'BA' => 'Bahia',
               'CE' => 'Ceará',
               'DF' => 'Distrito Federal',
               'ES' => 'Espírito Santo',
               'GO' => 'Goiás',
               'MA' => 'Maranhão',
               'MT' => 'Mato Grosso',
               'MS' => 'Mato Grosso do Sul',
               'MG' => 'Minas Gerais',
               'PA' => 'Pará',
               'PB' => 'Paraíba',
               'PR' => 'Paraná',
               'PE' => 'Pernambuco',
               'PI' => 'Piauí',
               'RJ' => 'Rio de Janeiro',
               'RN' => 'Rio Grande do Norte',
               'RO' => 'Rondônia',
               'RS' => 'Rio Grande do Sul',
               'RR' => 'Roraima',
               'SC' => 'Santa Catarina',
               'SE' => 'Sergipe',
               'SP' => 'São Paulo',
               'TO' => 'Tocantin'
               ), Input::old('uf'), array('class' => 'form-control', 'required')) }}
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            {{ Form::label('habitantes', 'Habitantes') }}
            {{ Form::text('habitantes', Input::old('habitantes'), array('class' => 'form-control text-center', 'placeholder' => 'Busca Automática', 'disabled')) }}
         </div>
      </div>
      <div class="col-md-3">
         <div class="form-group">
            {{ Form::label('veiculos', 'Veículos') }}
            {{ Form::text('veiculos', Input::old('veiculos'), array('class' => 'form-control text-center', 'placeholder' => 'Busca Automática', 'disabled')) }}
         </div>
      </div>
   </div>

   <div class="row paddings-conteudo">

      <div class="col-xs-12">
         {{ Form::button('<i class="fa fa-plus"></i> Cadastrar', array('type' => 'submit', 'class' => 'btn btn-success btn-in-col pull-right margin-left-15')) }}

         <a href="{{ URL::to('cidades') }}" class="btn btn-warning btn-in-col pull-right">
            <i class="fa fa-remove"></i> Calcelar
         </a>
      </div>

   </div>

{{ Form::close() }}

@stop
