@extends('layouts.master')


@section('javascripts')
@stop

@section('conteudo')

<!-- Header Bar -->
<div class="row header">
   <div class="col-xs-12">
      <div class="meta pull-left">
         <div class="page">
            {{ $representante->nome }} {{ $representante->sobrenome }}
         </div>
         <div class="breadcrumb-links">
            Home / Usuários / {{ $representante->nome }} {{ $representante->sobrenome }}
         </div>
      </div>
   </div>
</div>
<!-- End Header Bar -->


<div class="row paddings-conteudo">
   <div class="col-xs-12">

      <!--
      <a href="{{ URL::to('representantes/create') }}">
         <button type="button" class="btn btn-success">Adicionar Representante</button>
      </a>
      -->

      <div class="jumbotron text-center">
         <h2>{{ $representante->nome }}</h2>
         <p>
            <strong>Sobrenome:</strong> {{ $representante->sobrenome }}<br>
            <strong>Email:</strong> {{ $representante->email }}
         </p>
      </div>
   </div>
</div>

@stop
