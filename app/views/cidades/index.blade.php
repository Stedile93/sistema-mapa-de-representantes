@extends('layouts.master')


@section('javascripts')
@stop

@section('conteudo')

<!-- Header Bar -->
<div class="row header">
   <div class="col-xs-8">
      <div class="meta pull-left">
         <div class="page">
            Cidades
         </div>
         <div class="breadcrumb-links">
            Home / Cidades
         </div>
      </div>
   </div>

   <div class="col-xs-4 btn-header">
      <a class="pull-right" href="{{ URL::to('cidades/create') }}">
         Adicionar Cidade <i class="fa fa-plus fa-lg"></i>
      </a>
   </div>
</div>
<!-- End Header Bar -->


<div class="row paddings-conteudo">
   <div class="col-xs-12">

      {{ Form::open(array('url' => '/cidades', 'class' => 'buscar-ajax', 'method' => 'GET', 'autocomplete' => 'off')) }}
         <div class="form-group input-group">
            {{ Form::text('buscar', '', array('class' => 'form-control', 'placeholder' => 'Faça sua busca')) }}
            <span class="input-group-btn">
               {{ Form::button('<i class="fa fa-search"></i>', array('type' => 'submit', 'class' => 'btn btn-default')) }}
            </span>
         </div>
      {{ Form::close() }}

      <br>

      @if($termoBuscado != null)
      <h4>Termo buscado: "{{$termoBuscado}}" | {{$cidades->count()}} resultados encontrados</h4>
      @else
      <h4>Total de categorias cadastradas: {{$cidades->count()}}</h4>
      @endif

      <table class="table table-striped">
         <thead>
            <tr>
               <td class="coluna-acoes text-center">Ações</td>
               <td>Cidade</td>
               <td class="text-center">UF</td>
               <td class="text-center">Habitantes</td>
               <td class="text-center">Veículos</td>
               <td class="text-center">Status</td>
            </tr>
         </thead>
         <tbody>
            @foreach($cidades as $key => $value)
            <tr>
               <td class="text-center">
                  <a class="hover-verde pull-left margin-left-15" href="{{ URL::to('cidades/' . $value->id . '/edit') }}" title="Editar">
                     <i class="fa fa-edit fa-lg"></i> Ver/Editar
                  </a>
               </td>

               <td>{{ $value->nome }}</td>
               <td class="text-center">{{ $value->uf }}</td>
               <td class="text-center">
                  @if ($value->habitantes > 0)
                     {{ $value->habitantes }}
                  @else
                     Não Encontrado
                  @endif
               </td>
               <td class="text-center">
                  @if ($value->veiculos > 0)
                     {{ $value->veiculos }}
                  @else
                     Não Encontrado
                  @endif
               </td>
               <td class="text-center">
                  @if ($value->status == 1)
                     <span class="status-verde">Ativa</span>
                  @else
                     <span class="status-azul">Inativa</span>
                  @endif
               </td>
            </tr>
            @endforeach
         </tbody>
      </table>

      {{ $cidades->links() }}
   </div>
</div>

@stop
