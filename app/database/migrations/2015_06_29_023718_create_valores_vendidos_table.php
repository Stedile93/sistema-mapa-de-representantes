<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValoresVendidosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('valores_vendidos', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('id_usuario')->unsigned();
	      $table->foreign('id_usuario')->references('id')->on('usuarios');

			$table->integer('id_cidade')->unsigned();
	      $table->foreign('id_cidade')->references('id')->on('cidades');

			$table->date('data');
			$table->decimal('valor_vendido', 11, 2);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('valores_vendidos');
	}

}
