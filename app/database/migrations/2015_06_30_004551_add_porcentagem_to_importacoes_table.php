<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPorcentagemToImportacoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('importacoes', function(Blueprint $table)
		{
			$table->decimal('porcentagem', 11, 2);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('importacoes', function(Blueprint $table)
		{
			$table->dropColumn('porcentagem');
		});
	}

}
