<!doctype html>
<html lang="pt-BR">

   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <meta name="description" content="Sistema de Estatísticas e Relatórios Lubrilages">
      <meta name="author" content="Ueek Agência Digital">

      <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

      <title>Sistema - LUBRILAGES E NOVOLUBRI COM. LUBRIFICANTE</title>

      <!-- STYLES -->
      {{ HTML::style('css/bootstrap.min.css', array('type' => 'text/css')) }}
      {{ HTML::style('css/bootstrap-datepicker3.min.css', array('type' => 'text/css')) }}
      {{ HTML::style('css/bootbox.css', array('type' => 'text/css')) }}
      {{ HTML::style('css/font-awesome.min.css', array('type' => 'text/css')) }}
      {{ HTML::style('css/rdash.css', array('type' => 'text/css')) }}
      {{ HTML::style('css/main.css', array('type' => 'text/css')) }}

      @yield('css')

      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
         <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
         <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>

   <body>

      <div id="content-wrapper">
         <main class="page-content">

            <!-- Main Content -->
            @yield('conteudo')

         </main><!-- End Page Content -->
      </div>



      <!-- SCRIPTS -->
      {{ HTML::script('js/jquery.min.js', array('type' => 'text/javascript')) }}
      {{ HTML::script('js/bootstrap.min.js', array('type' => 'text/javascript')) }}
      {{ HTML::script('js/bootbox.min.js', array('type' => 'text/javascript')) }}
      {{ HTML::script('js/jquery.cookie.js', array('type' => 'text/javascript')) }}

      {{ HTML::script('js/bootstrap-datepicker.min.js', array('type' => 'text/javascript')) }}
      {{ HTML::script('js/bootstrap-datepicker.pt-BR.min.js', array('type' => 'text/javascript')) }}

      @yield('javascripts')

   </body>

</html>
