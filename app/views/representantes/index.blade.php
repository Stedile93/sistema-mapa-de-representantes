@extends('layouts.master')


@section('javascripts')
@stop

@section('conteudo')

<!-- Header Bar -->
<div class="row header">
   <div class="col-xs-8">
      <div class="meta pull-left">
         <div class="page">
            Representantes
         </div>
         <div class="breadcrumb-links">
            Home / Representantes
         </div>
      </div>
   </div>

   <div class="col-xs-4 btn-header">
      <a class="pull-right" href="{{ URL::to('representantes/create') }}">
         Adicionar Representante <i class="fa fa-plus fa-lg"></i>
      </a>
   </div>
</div>
<!-- End Header Bar -->


<div class="row paddings-conteudo">
   <div class="col-xs-12">

      {{ Form::open(array('url' => '/representantes', 'class' => 'buscar-ajax', 'method' => 'GET', 'autocomplete' => 'off')) }}
         <div class="form-group input-group">
            {{ Form::text('buscar', '', array('class' => 'form-control', 'placeholder' => 'Faça sua busca')) }}
            <span class="input-group-btn">
               {{ Form::button('<i class="fa fa-search"></i>', array('type' => 'submit', 'class' => 'btn btn-default')) }}
            </span>
         </div>
      {{ Form::close() }}

      <br>

      @if($termoBuscado != null)
      <h4>Termo buscado: "{{$termoBuscado}}" | {{$representantes->count()}} resultados encontrados</h4>
      @else
      <h4>Total de categorias cadastradas: {{$representantes->count()}}</h4>
      @endif

      <table class="table table-striped">
         <thead>
            <tr>
               <td class="coluna-acoes text-center">Ações</td>
               <td>Nome</td>
               <td>Login</td>
               <td>E-mail</td>
               <td>Endereço</td>
               <td class="text-center">Cor</td>
            </tr>
         </thead>
         <tbody>
            @foreach($representantes as $key => $value)
            <tr>
               <td class="text-center">
                  <a class="hover-verde pull-left margin-left-15" href="{{ URL::to('representantes/' . $value->id . '/edit') }}" title="Editar">
                     <i class="fa fa-edit fa-lg"></i> Ver/Editar
                  </a>
               </td>

               <td>{{ $value->nome }}</td>
               <td>{{ $value->login }}</td>
               <td>{{ $value->email }}</td>
               <td>
                  @if(!empty($value->cidade))
                     {{ $value->cidade }} @if(!empty($value->uf)) - {{$value->uf}} @endif
                  @else
                     Não informado
                  @endif
               </td>
               <td class="text-center">
                  <i class="fa fa-circle fa-lg" style="color: {{ $value->cor }};"></i>
               </td>
            </tr>
            @endforeach
         </tbody>
      </table>

      {{ $representantes->links() }}
   </div>
</div>

@stop
